## Banana Bomb
_A fast, safe, code generated event bus for Android_

#### Why

Other popular message brokers for Android are using reflection.  
We dont like that.  
With code generation we also achieve additional type safety.
For example, if we are listening for events that we forgot to send - the code wont even compile!

#### Installation

```
compile 'se.wollan:bananabomb:<version>'
annotationProcessor 'se.wollan:bananabomb-compiler:<version>'
``` 
Get latest stable version from [here](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22se.wollan%22%20AND%20(a%3A%22bananabomb%22%20OR%20a%3A%22bananabomb-compiler%22)).

#### Usage

Let's first hit you with a working example.

```
package com.example;

//1. Define your events
@Banana
class ActivityDestroyed { } 

//2. Define your event bus 
//(Radar, Detonator and EventBusImpl will be generated for you)
@BananaBomb
interface EventBus extends com.example.Radar, com.example.Detonator {
    EventBus INSTANCE = new EventBusImpl();
}

//3. Add class listening for events
class BackgroundService {

    BackgroundService() {
        EventBus.INSTANCE.lockTargets(this); //register
    }

    @BananaTarget
    void on(ActivityDestroyed event) {
        EventBus.INSTANCE.unlockTargets(this); //unregister
    }
}

//4. Add a class emitting events
public class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new BackgroundService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.INSTANCE.detonate(new ActivityDestroyed());
    }
}
```

At this point you are probably wondering what's up with all these `@Banana`s and `@BananaBombs`. Well, the terminology is loosely borrowed from the suite of Worms PC games. The banana bomb is a type of a cluster bomb and when detonated the bananas will fly all over and hit multiple targets. 

So, when we speak of banans we are really talking of user defined event classes annotated with `@Banana` like this one
```
@Banana
class ActivityDestroyed { } 
```
Our annotation processor will at build time pick these up and generate a detonator
```
public interface ActivityDestroyedDetonator { // generated!
    int detonate(ActivityDestroyed banana);
}
```
which you can use to send, or _detonate_ these events.

The event handler, or _banana target_, are methods annotated with `@BananaTarget`.
```
@BananaTarget
void on(ActivityDestroyed event) {
}
```
For each class containing at least one target (we call these bombing ranges) a _radar_ will be generated like so:
```
public interface BackgroundServiceRadar { // generated!
    BackgroundServiceRadar lockTargets(BackgroundService bombingRange);
    BackgroundServiceRadar lockTargets(BackgroundService bombingRange, Looper looper);
    BackgroundServiceRadar unlockTargets(BackgroundService bombingRange);
}
```
Using this interface you can subscribe (_lock targets_) and unsubscribe (_unlock targets_) to bananas at runtime.
The only caveat is that the thread calling lock must be a [looper](https://developer.android.com/reference/android/os/Looper.html) thread like the UI thread or a [handler thread](https://developer.android.com/reference/android/os/HandlerThread.html). The reason for this is that all detonated bananas are queued on the respective thread's event queue for async processing.

For convenience, all radars and detonators are grouped together by common `Radar` and `Detonator` interfaces:
```
public interface Radar extends // generated!
    com.example.BackgroundServiceRadar
    /* com.example.SomeOtherRadar etc */ {
}
public interface Detonator extends // generated!
    com.example.ActivityDestroyedDetonator
    /* com.exmaple.ActivityCreatedDetonator etc */ {
}
```
The reason for this is that the actual event bus implementation is specified by an interface annotated with `@BananaBomb`
```
@BananaBomb
interface EventBus extends 
    com.example.Radar, 
    com.example.Detonator {
}
```
where extending all radars and detonators that should be handled by this bomb. So for creating a global bomb containing all bananas and targets in the application, just let the bomb extend the common radar and detonator. The resulting implmentation are suffixed with "Impl" like "EventBusImpl" and are generated in the same package as the bomb interface. 

Be aware that the code won't compile if not all bananas have a target (or vice versa). Test this by removing the radar or detonator from the bomb. 

It is possible to setup a more specific event bus by extending specific radars/detonators or defining your own banana cluster by extending the `@Banana` annotation (see below).

#### Exception handling

Per default the banana bomb won't catch anything, matching the default behaviour of an Android application. Meaning that any unhandled runtime exception will crash the process. To change this, create a target to handle the `ExceptionalBanana`:
```
public class GlobalExceptionHandler {

    public GlobalExceptionHandler(GlobalExceptionHandlerRadar radar) {
        radar.lockTargets(this);
    }

    @BananaTarget
    void on(ExceptionalBanana exception) {

        Log.e("GlobalExceptionHandler", "Oooops! Houston, we've got a problem: " + exception.getException().getMessage());

        if (BuildConfig.DEBUG) { //dont crash in production
            exception.rethrow();
        }
    }
}
```

#### Wildcarding

It is possible to create wildcard targets to catch all bananas that can be implicitly casted to target type. For
```
interface BaseEvent { }

@Banana 
class ClickEvent implements BaseEvent { }

@BananaTarget
void on(BaseEvent event) {
}

detonator.detonate(new ClickEvent());
```
the target will catch click event due to that the `ClickEvent` are assignable to `BaseEvent` (without explicit cast). This means that creating an object target
```
@BananaTarget
void on(Object event) {
}
```
will match all bananas for a given banana bomb.

#### Clusters

Clusters are logical groupings of bananas to make it easier to create smaller context specific bombs without having to maintain a long list of interfaces to extend. Define a login specific cluster by 
```
@Banana
@Target({ ElementType.TYPE })
public @interface LoginMessage {
}
```
and use it like so
```
@LoginMessage
public class LoginFailed { }

@LoginMessage
public class LoginSucceeded { }

public class LoginPresenter {
    @BananaTarget
    void on(LoginSucceeded event) { }
}
```
This will generate
```
public interface LoginMessageDetonator extends // generated!
    com.example.LoginSucceededDetonator,
    com.example.LoginFailedDetonator {
}
public interface LoginMessageRadar extends // generated!
    com.example.LoginPresenterRadar {
}
```
Which will make it easy to setup the cluster bomb
```
@BananaBomb
public interface LoginBus extends
        com.example.LoginMessageRadar,
        com.example.LoginMessageDetonator {
}
```
As you can see, the default `Radar`/`Detonator`/`@Banana` are together creating their own app-global cluster.

#### TODO

##### LOWER PRIO

- RoutingKey so responses can be routed back to client (to support concurrent Ping-Pong) (Needs some serious thinking-about!)
- Add source annotations when writing filer.createSource where possible (be sure so it doesnt break though!)
- Optimize performance, like cache Template dont read it every time
- If adding your own lockTargets to @BananaBomb interface, your dont need to add all three methods, only one or two should also work (dont gen impl for all three in that case).
- Support for nested classes, especially for Bananas.
- Log middleware for debugging
- Does value objects like int work as bananas and for Object wildcarding?
- Generics. Generic bananas GenericBanana<T> could yield GenericBananaDetonator<T>?

##### REFACTOR

- Use Dagger instead in manual injection.

##### SHELVED

- getDefault() on banana bomb

## Donations

[Donate XMR here](monero:89iiHE2pbx7HdoZRTMmrefFUy6NqDC3TkNBbfAZiJaxvarTEmGGBoYogEJFMWgR7BVdWfgeDin5HQK3v7wca7XRf5DToozC) 💜
