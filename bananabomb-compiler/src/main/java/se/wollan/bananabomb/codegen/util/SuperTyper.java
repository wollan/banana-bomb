package se.wollan.bananabomb.codegen.util;

import com.google.common.collect.ImmutableSet;

import javax.lang.model.type.TypeMirror;

import se.wollan.bananabomb.codegen.model.CanonicalName;

/**
 * Created by aw on 2017-06-10.
 */

public interface SuperTyper {

    /**
     * @param self the subject type
     * @return all super types all the way to the top, generated or not, both interface
     * and classes. Except self, even for top level.
     */
    ImmutableSet<CanonicalName> getAllSupertypes(TypeMirror self);
}
