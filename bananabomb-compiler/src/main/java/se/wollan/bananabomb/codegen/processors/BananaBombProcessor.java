package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;

public class BananaBombProcessor extends AbstractProcessor {

    private ProcessingCoordinator coordinator;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        ProcessInjector.injectInto(this, processingEnvironment);
    }

    void inject(ProcessingCoordinator coordinator) {
        this.coordinator = coordinator;
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment env) {
        ImmutableSet<BananaBomb> bananaBombs = BananaBomb.getAnnotatedBombs(env);
        coordinator.coordinateBananaBombs(bananaBombs);
        return true;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_7;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return ImmutableSet.of(se.wollan.bananabomb.BananaBomb.class.getCanonicalName());
    }
}
