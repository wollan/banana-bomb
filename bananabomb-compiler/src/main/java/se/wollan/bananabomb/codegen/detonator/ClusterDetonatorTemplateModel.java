package se.wollan.bananabomb.codegen.detonator;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.model.Cluster;
import se.wollan.bananabomb.codegen.processors.BananaProcessor;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.DateFormats.nowAsISO8601;

class ClusterDetonatorTemplateModel {

    final CanonicalName processor = toCanonical(BananaProcessor.class);
    final String date = nowAsISO8601();
    final CanonicalName interfaceName;
    final ImmutableList<ImplementsModel> impl;
    final Cluster cluster;
    boolean anyImpls() { return !impl.isEmpty(); }

    ClusterDetonatorTemplateModel(Cluster cluster, ImmutableSet<Banana> bananas) {
        ImmutableList.Builder<ImplementsModel> builder = ImmutableList.builder();
        ImmutableList<Banana> asList = bananas.asList();
        for (int i = 0; i < asList.size(); i++) {
            Banana banana = asList.get(i);
            if (!banana.belongsTo(cluster)) {
                throw new IllegalArgumentException("banana "+banana+" does not belong to cluster "+cluster);
            }
            builder.add(new ImplementsModel(banana, i < bananas.size() - 1));
        }

        this.interfaceName = cluster.getCanonical().withSuffix("Detonator");
        this.impl = builder.build();
        this.cluster = cluster;
    }

    static class ImplementsModel {
        final Banana banana;
        final boolean comma;

        ImplementsModel(Banana banana, boolean comma) {
            this.banana = banana;
            this.comma = comma;
        }
    }
}
