package se.wollan.bananabomb.codegen.radar;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.model.Cluster;
import se.wollan.bananabomb.codegen.processors.BananaTargetProcessor;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.DateFormats.nowAsISO8601;

/**
 * Created by aw on 2017-04-23.
 */

class ClusterRadarTemplateModel {

    final CanonicalName processor = toCanonical(BananaTargetProcessor.class);
    final String date = nowAsISO8601();
    final CanonicalName interfaceName;
    final Cluster cluster;
    final ImmutableList<ImplementsModel> impl;
    boolean anyImpls() { return !impl.isEmpty(); }

    ClusterRadarTemplateModel(Cluster cluster, ImmutableSet<BombingRange> bombingRanges) {

        ImmutableList.Builder<ImplementsModel> builder = ImmutableList.builder();
        ImmutableList<BombingRange> asList = bombingRanges.asList();
        for (int i = 0; i < asList.size(); i++) {
            builder.add(new ImplementsModel(asList.get(i), i < bombingRanges.size() - 1));
        }

        this.interfaceName = cluster.getCanonical().withSuffix("Radar");
        this.cluster = throwIfNull(cluster, "cluster");
        this.impl = builder.build();
    }

    static class ImplementsModel {
        final BombingRange bombingRange;
        final boolean comma;

        ImplementsModel(BombingRange bombingRange, boolean comma) {
            this.bombingRange = bombingRange;
            this.comma = comma;
        }
    }

}
