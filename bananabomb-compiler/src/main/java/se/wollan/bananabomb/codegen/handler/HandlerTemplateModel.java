package se.wollan.bananabomb.codegen.handler;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.BananaBombProcessor;
import se.wollan.bananabomb.codegen.processors.BananaTargetProcessor;

import static se.wollan.bananabomb.codegen.model.BananaTarget.getSingleBomingRangeOrThrow;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.DateFormats.nowAsISO8601;

/**
 * Created by aw on 2017-04-23.
 */

@SuppressWarnings("WeakerAccess")
class HandlerTemplateModel {

    final CanonicalName processor = toCanonical(BananaTargetProcessor.class);
    final String date = nowAsISO8601();
    final CanonicalName className;
    final BombingRange bombingRange;
    final ImmutableList<TargetModel> targets;
    final ImmutableList<TargetModel> wontCatchTargets;
    boolean anyWontCatchTargets() { return !wontCatchTargets.isEmpty(); }
    final ImmutableList<TargetModel> willCatchTargets;
    boolean anyWillCatchTargets() { return !willCatchTargets.isEmpty(); }
    final ImmutableList<SendMethodModel> sendMethods;


    HandlerTemplateModel(ImmutableSet<BananaTarget> bananaTargets) {
        this.targets = createTargetModels(bananaTargets);
        this.wontCatchTargets = filterOut(targets, Integer.MIN_VALUE, -1);
        this.willCatchTargets = filterOut(targets, 0, Integer.MAX_VALUE);
        this.bombingRange = getSingleBomingRangeOrThrow(bananaTargets);
        this.className = bombingRange.getCanonical().withSuffix("BananaHandler");
        this.sendMethods = createSendMethodModels(targets);
    }

    static class TargetModel {
        final BananaTarget bananaTarget;
        final int id;

        public TargetModel(BananaTarget bananaTarget, int id) {
            this.bananaTarget = bananaTarget;
            this.id = id;
        }
    }

    static class SendMethodModel {
        final Banana banana;
        final ImmutableList<TargetModel> targets;

        public SendMethodModel(Banana banana, ImmutableList<TargetModel> targets) {
            this.banana = banana;
            this.targets = targets;
        }
    }

    private ImmutableList<TargetModel> createTargetModels(ImmutableSet<BananaTarget> bananaTargets) {
        ImmutableList.Builder<TargetModel> targetModelBuilder = ImmutableList.builder();
        int wontCatchId = -1, willCatchId = 0;
        for (BananaTarget bananaTarget : bananaTargets) {
            int id = bananaTarget.getBanana().isExceptional() ? wontCatchId-- : willCatchId++;
            targetModelBuilder.add(new TargetModel(bananaTarget, id));
        }
        return targetModelBuilder.build();
    }

    private static ImmutableList<TargetModel> filterOut(ImmutableList<TargetModel> targets, final int minId, final int maxId) {
        return FluentIterable.from(targets)
                .filter(new Predicate<TargetModel>() {
                    @Override
                    public boolean apply(TargetModel targetModel) {
                        return minId <= targetModel.id && targetModel.id <= maxId;
                    }
                })
                .toList();
    }

    private static ImmutableList<SendMethodModel> createSendMethodModels(final ImmutableList<TargetModel> targets) {
        ImmutableSet<Banana> bananas = uniqueBananas(targets);
        return FluentIterable.from(bananas)
                .transform(new Function<Banana, SendMethodModel>() {
                    @Override
                    public SendMethodModel apply(final Banana banana) {
                        ImmutableList<TargetModel> assignableTargets = FluentIterable.from(targets)
                                .filter(new Predicate<TargetModel>() {
                                    @Override
                                    public boolean apply(TargetModel targetModel) {
                                        return banana.isAssignableTo(targetModel.bananaTarget.getBanana());
                                    }
                                })
                                .toList();
                        return new SendMethodModel(banana, assignableTargets);
                    }
                }).toList();
    }

    private static ImmutableSet<Banana> uniqueBananas(ImmutableList<TargetModel> targets) {
        return FluentIterable.from(targets)
                .transform(new Function<TargetModel, Banana>() {
                    @Override
                    public Banana apply(TargetModel input) {
                        return input.bananaTarget.getBanana();
                    }
                })
                .toSet();
    }

}
