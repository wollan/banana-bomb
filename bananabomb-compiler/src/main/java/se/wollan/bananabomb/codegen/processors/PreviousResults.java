package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;

import se.wollan.bananabomb.codegen.model.CanonicalName;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.appendOrThrow;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;

public class PreviousResults {
    private final ImmutableList<FactorResult> results;

    public PreviousResults(ImmutableList<FactorResult> results) {
        this.results = throwIfNull(results, "results");
    }

    public static PreviousResults empty() {
        return new PreviousResults(ImmutableList.<FactorResult>of());
    }

    public PreviousResults append(FactorResult result) {
        return new PreviousResults(appendOrThrow(results, result));
    }

    public GeneratedFileResult getResultByGeneratedFileNameOrNull(CanonicalName canonicalName) {
        for (FactorResult factorResult : results) {
            for (GeneratedFileResult generatedFileResult : factorResult.getFileResults()) {
                if (generatedFileResult.getCanonicalNameOfFile().equals(canonicalName)) {
                    return generatedFileResult;
                }
            }
        }
        return null;
    }
}
