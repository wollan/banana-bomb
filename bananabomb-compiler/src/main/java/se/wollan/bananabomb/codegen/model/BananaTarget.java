package se.wollan.bananabomb.codegen.model;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;

import se.wollan.bananabomb.codegen.util.SuperTyper;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNotSizeOne;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrContainsWhitespace;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrWhitespace;

/**
 * Created by aw on 2017-04-23.
 */
public class BananaTarget {

    final BombingRange bombingRange;
    final String methodName;
    final Banana banana;

    public BananaTarget(SuperTyper superTyper, ExecutableElement targetMethod) {
        VariableElement banana = throwIfNotSizeOne(targetMethod.getParameters(), "method parameters").get(0);
        bombingRange = new BombingRange(targetMethod.getEnclosingElement());
        methodName = targetMethod.getSimpleName().toString();
        this.banana = new Banana(superTyper, banana.asType());
    }

    public BananaTarget(BombingRange bombingRange, String methodName, Banana banana) {
        this.bombingRange = throwIfNull(bombingRange, "bombingRange");
        this.methodName = throwIfNullOrContainsWhitespace(methodName, "methodName");
        this.banana = throwIfNull(banana, "banana");
    }

    public BombingRange getBombingRange() {
        return bombingRange;
    }

    public String getMethodName() {
        return methodName;
    }

    @Override
    public String toString() {
        return bombingRange.toString() + "." + methodName + "(" + banana.toString() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BananaTarget that = (BananaTarget) o;

        if (!bombingRange.equals(that.bombingRange)) return false;
        if (!methodName.equals(that.methodName)) return false;
        return banana.equals(that.banana);
    }

    @Override
    public int hashCode() {
        int result = bombingRange.hashCode();
        result = 31 * result + methodName.hashCode();
        result = 31 * result + banana.hashCode();
        return result;
    }

    public static ImmutableSet<BombingRange> uniqueBombingRanges(Iterable<BananaTarget> bananaTargets) {

        ImmutableSet.Builder<BombingRange> builder = ImmutableSet.builder();
        for (BananaTarget bananaTarget : bananaTargets) {
            builder.add(bananaTarget.getBombingRange());
        }
        return builder.build();
    }

    public static ImmutableSet<BananaTarget> getAnnotatedTargets(SuperTyper superTyper, RoundEnvironment env) {
        Collection<? extends Element> annotatedElements =
                env.getElementsAnnotatedWith(se.wollan.bananabomb.BananaTarget.class);

        ImmutableSet.Builder<BananaTarget> builder = ImmutableSet.builder();
        for (ExecutableElement method : ElementFilter.methodsIn(annotatedElements)) {
            builder.add(new BananaTarget(superTyper, method));
        }
        return builder.build();
    }

    public static BombingRange getSingleBomingRangeOrThrow(Iterable<BananaTarget> bananaTargets) {
        BombingRange bombingRange = null;
        for (BananaTarget bananaTarget : bananaTargets) {
            if (bananaTarget == null) {
                throw new IllegalArgumentException("one bananaTarget is null!");
            }
            if (bombingRange == null) {
                bombingRange = bananaTarget.getBombingRange();
                continue;
            }
            if (!bombingRange.equals(bananaTarget.getBombingRange())) {
                throw new IllegalArgumentException(bombingRange + " isnt equal to " + bananaTarget.getBombingRange());
            }
        }

        if (bombingRange == null) {
            throw new IllegalArgumentException("could not find any target class!");
        }

        return bombingRange;
    }

    public Banana getBanana() {
        return banana;
    }

    public static ImmutableSet<BananaTarget> targetsWithinRange(
            Iterable<BananaTarget> allBananaTargets, BombingRange bombingRange) {

        ImmutableSet.Builder<BananaTarget> builder = ImmutableSet.builder();
        for (BananaTarget bananaTarget : allBananaTargets) {
            if (bananaTarget.getBombingRange().equals(bombingRange)) {
                builder.add(bananaTarget);
            }
        }
        return builder.build();
    }

    public static ImmutableSet<BombingRange> getBombingRangesMatchingBanana(
            Iterable<BananaTarget> bananaTargets, Banana banana) {

        ImmutableSet.Builder<BombingRange> builder = ImmutableSet.builder();
        for (BananaTarget targetInfo : bananaTargets) {
            if (banana.isAssignableTo(targetInfo.getBanana())) {
                builder.add(targetInfo.getBombingRange());
            }
        }

        return builder.build();
    }

    public static boolean containsAssignableBanana(Iterable<BananaTarget> targets, Banana banana) {
        for (BananaTarget target : targets) {
            if (banana.isAssignableTo(target.getBanana())) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasRottenTarget(Iterable<BananaTarget> bananaTargets) {
        for (BananaTarget target : bananaTargets) {
            if (target.getBanana().isRotten()) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasExceptionalTarget(Iterable<BananaTarget> bananaTargets) {
        for (BananaTarget target : bananaTargets) {
            if (target.getBanana().isExceptional()) {
                return true;
            }
        }
        return false;
    }

    public static ImmutableSet<BananaTarget> enrichBananas(ImmutableSet<BananaTarget> bananaTargets,
                                                           ImmutableSet<Banana> bananas) {
        throwIfNull(bananas, "bananas");
        throwIfNull(bananaTargets, "bananaTargets");

        ImmutableSet.Builder<BananaTarget> out = ImmutableSet.builder();
        for (BananaTarget target : bananaTargets) {
            throwIfNull(target, "banana target");
            Banana richBanana = null;
            for (Banana b : bananas) {
                throwIfNull(b, "banana");
                if (b.equals(target.banana)) {
                    richBanana = b;
                }
            }
            if (richBanana != null) {
                out.add(target.enrichBananas(richBanana));
            } else {
                out.add(target);
            }
        }
        return out.build();
    }

    private BananaTarget enrichBananas(Banana richBanana) {
        if (throwIfNull(richBanana, "richBanana").unknownClusters()) {
            throw new IllegalArgumentException("supposedly rich banana has unknown clusters!");
        }
        if (!banana.unknownClusters()) {
            throw new IllegalArgumentException("supposedly not rich banana has known clusters");
        }
        return new BananaTarget(bombingRange, methodName, richBanana);
    }
}

