package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.io.IOException;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.CanonicalName;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrWhitespace;

/**
 * Created by aw on 2017-04-24.
 */

public interface BombFactory {

    FactorResult factor(ImmutableSet<Banana> bananas,
                        ImmutableSet<BananaTarget> bananaTargets,
                        ImmutableSet<BananaBomb> bananaBombs,
                        PreviousResults previousResults)
                        throws IOException;
}

