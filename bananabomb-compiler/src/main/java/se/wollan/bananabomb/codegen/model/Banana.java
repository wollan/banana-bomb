package se.wollan.bananabomb.codegen.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import javax.lang.model.type.TypeMirror;

import se.wollan.bananabomb.ExceptionalBanana;
import se.wollan.bananabomb.RottenBanana;
import se.wollan.bananabomb.codegen.util.ElementAndAnnotations;
import se.wollan.bananabomb.codegen.util.SuperAnnotator;
import se.wollan.bananabomb.codegen.util.SuperTyper;

import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfContains;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNotContains;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNotEmpty;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ImmutableUtils.concat;

public class Banana {

    public static final Banana JAVA_OBJECT = new Banana(Object.class);
    public static final Banana ROTTEN = new Banana(RottenBanana.class);
    public static final Banana EXCEPTIONAL = new Banana(ExceptionalBanana.class);

    private final CanonicalName canonical;
    private final ImmutableSet<Banana> assignableTo; //does not contains this banana, but of course we are assignable to ourself!
    private final ImmutableSet<Cluster> belongsTo; //empty means we dont know which clusters, so be careful!

    public Banana(String canonicalName) {
        this(toCanonical(canonicalName));
    }

    public Banana(Class klass) {
        this(toCanonical(klass.getCanonicalName()));
    }

    public Banana(CanonicalName canonical) {
        this(canonical, canonical.isTopLevel()
                ? ImmutableSet.<Banana>of()
                : ImmutableSet.of(Banana.JAVA_OBJECT), ImmutableSet.of());
    }

    public Banana(SuperTyper superTyper, TypeMirror mirror) {
        this(superTyper, mirror, ImmutableSet.of());
    }

    public Banana(SuperTyper superTyper, TypeMirror mirror, ImmutableSet<Cluster> belongsTo) {
        this(toCanonical(mirror.toString()), superTyper
                .getAllSupertypes(mirror)
                .stream()
                .map(Banana::toBanana)
                .collect(toImmutableSet()), belongsTo);
    }

    public Banana(String canonicalName, ImmutableSet<Banana> assignableTo) {
        this(toCanonical(canonicalName), assignableTo, ImmutableSet.of());
    }

    public Banana(CanonicalName canonical, ImmutableSet<Banana> assignableTo, ImmutableSet<Cluster> belongsTo) {
        this.canonical = throwIfNull(canonical, "canonical");
        this.assignableTo = canonical.isTopLevel()
                ? throwIfNotEmpty(assignableTo, "assignableTo")
                : throwIfContains(throwIfNotContains(assignableTo, Banana.JAVA_OBJECT, "assignableTo"), this, "assignableTo");

        throwIfNull(belongsTo, "belongsTo");
        if (!belongsTo.isEmpty()) {
            throwIfNotContains(belongsTo, Cluster.TOP_LEVEL, "belongsTo");
        }
        this.belongsTo = belongsTo;
    }

    public static ImmutableSet<Banana> getAnnotatedBananas(SuperTyper superTyper, SuperAnnotator superAnnotator) {
        ImmutableSet<ElementAndAnnotations> annotatedElements = superAnnotator
                .getElementsAnnotatedWithIncludingSubAnnotations(se.wollan.bananabomb.Banana.class);

        ImmutableSet.Builder<Banana> bananaTypeBuilder = ImmutableSet.builder();
        for (ElementAndAnnotations type : annotatedElements) {
            bananaTypeBuilder.add(new Banana(superTyper, type.getElement().asType(),
                    type.getAnnotations().stream().map(Cluster::toCluster).collect(toImmutableSet())));
        }
        return bananaTypeBuilder.build();
    }

    public Banana extend(String subClassName) {
        return new Banana(toCanonical(subClassName), concat(assignableTo, this), belongsTo);
    }

    public boolean isAssignableTo(Banana assignableToBanana) {
        throwIfNull(assignableToBanana, "assignableToBanana");
        return this.equals(assignableToBanana) ||  assignableTo.contains(assignableToBanana);
    }

    public static Banana toBanana(CanonicalName canonicalName) {
        return new Banana(canonicalName);
    }

    @Override
    public String toString() {
        return canonical.toString();
    }

    public CanonicalName getCanonical() {
        return canonical;
    }

    public ImmutableSet<Banana> getAssignableTo() {
        return assignableTo;
    }

    public ImmutableSet<Cluster> getBelongsTo() {
        return belongsTo;
    }

    public boolean unknownClusters() {
        return belongsTo.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Banana banana = (Banana) o;

        return canonical.equals(banana.canonical);
    }

    @Override
    public int hashCode() {
        return canonical.hashCode();
    }

    public boolean isRotten() {
        return equals(ROTTEN);
    }

    public boolean isExceptional() {
        return equals(EXCEPTIONAL);
    }

    public static boolean containsAssignableTarget(Iterable<Banana> bananas, BananaTarget target) {
        for (Banana banana : bananas) {
            if(banana.isAssignableTo(target.getBanana())) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasRottenBanana(Iterable<Banana> bananas) {
        for (Banana banana : throwIfNull(bananas, "bananas")) {
            if (throwIfNull(banana, "banana").isRotten()) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasExceptionalBanana(Iterable<Banana> bananas) {
        for (Banana banana : throwIfNull(bananas, "bananas")) {
            if (throwIfNull(banana, "banana").isExceptional()) {
                return true;
            }
        }
        return false;
    }

    public boolean belongsTo(Cluster cluster) {
        return belongsTo.contains(throwIfNull(cluster, "cluster"));
    }
}
