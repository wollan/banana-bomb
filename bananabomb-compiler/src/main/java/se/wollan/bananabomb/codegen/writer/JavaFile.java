package se.wollan.bananabomb.codegen.writer;

import se.wollan.bananabomb.codegen.model.CanonicalName;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrWhitespace;

public class JavaFile {

    private final CanonicalName typeName;
    private final String fileContents;

    public JavaFile(CanonicalName typeName, String fileContents) {
        this.typeName = throwIfNull(typeName, "typeName");
        this.fileContents = throwIfNullOrWhitespace(fileContents, "fileContents");
    }

    public CanonicalName getTypeName() {
        return typeName;
    }

    public String getFileContents() {
        return fileContents;
    }
}
