package se.wollan.bananabomb.codegen.writer;

import java.io.IOException;

import javax.annotation.processing.FilerException;

public interface JavaFileWriter {

    void write(JavaFile javaFile) throws IOException;
}


