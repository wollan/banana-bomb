package se.wollan.bananabomb.codegen.util;

import java.io.IOException;

/**
 * Created by aw on 2017-05-01.
 */

public interface ResourceLoader {

    String loadResourceDataByName(String name) throws IOException;

}
