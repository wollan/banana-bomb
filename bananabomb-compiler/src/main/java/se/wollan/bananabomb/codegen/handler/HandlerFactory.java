package se.wollan.bananabomb.codegen.handler;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.io.IOException;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.processors.FactorResult;
import se.wollan.bananabomb.codegen.processors.GeneratedFileResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;
import se.wollan.bananabomb.codegen.templating.Template;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.util.ResourceLoader;
import se.wollan.bananabomb.codegen.writer.JavaFile;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;

import static se.wollan.bananabomb.codegen.model.BananaTarget.targetsWithinRange;
import static se.wollan.bananabomb.codegen.model.BananaTarget.uniqueBombingRanges;

/**
 * Created by aw on 2017-04-23.
 */

class HandlerFactory implements BombFactory {

    private final JavaFileWriter fileWriter;
    private final Templater templater;
    private final ResourceLoader resourceLoader;

    HandlerFactory(JavaFileWriter fileWriter, Templater templater, ResourceLoader resourceLoader) {
        this.fileWriter = fileWriter;
        this.templater = templater;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public FactorResult factor(ImmutableSet<Banana> bananas, ImmutableSet<BananaTarget> allBananaTargets,
                               ImmutableSet<BananaBomb> bananaBombs, PreviousResults previousResults) throws IOException {
        FactorResult result = FactorResult.create("HandlerFactory");

        ImmutableSet<BombingRange> uniqueRanges = uniqueBombingRanges(allBananaTargets);

        for (BombingRange bombingRange : uniqueRanges) {
            result = result.append(factorHandler(allBananaTargets, bombingRange));
        }
        return result;
    }

    private GeneratedFileResult factorHandler(ImmutableSet<BananaTarget> allBananaTargets, BombingRange bombingRange) throws IOException {
        ImmutableSet<BananaTarget> targets = targetsWithinRange(allBananaTargets, bombingRange);

        HandlerTemplateModel templateModel = new HandlerTemplateModel(targets);
        String templateData = resourceLoader.loadResourceDataByName("handler.mustache");
        Template template = new Template(templateData, templateModel.className);
        JavaFile javaFile = templater.render(template, templateModel);
        fileWriter.write(javaFile);

        return GeneratedFileResult.fromTargetsAndRange(allBananaTargets, bombingRange,
                templateModel.className);
    }
}
