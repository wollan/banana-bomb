package se.wollan.bananabomb.codegen.util;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by aw on 2017-05-01.
 */
public class ResourceLoaderImpl implements ResourceLoader {

    @Override
    public String loadResourceDataByName(String name) throws IOException {

        URL resource = ResourceLoaderImpl.class.getClassLoader().getResource(name);
        if (resource == null) {
            throw new IllegalArgumentException("Cannot find resource " + name);
        }

        try(InputStream inputStream = resource.openStream()) {
            final char[] buffer = new char[4096];
            final StringBuilder out = new StringBuilder();
            try(Reader in = new InputStreamReader(inputStream, Charsets.UTF_8)) {
                while (true){
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0) break;
                    out.append(buffer, 0, rsz);
                }
            }
            return out.toString();
        }
    }
}
