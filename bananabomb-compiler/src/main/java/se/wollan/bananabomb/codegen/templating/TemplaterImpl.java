package se.wollan.bananabomb.codegen.templating;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import java.io.StringReader;
import java.io.StringWriter;

import se.wollan.bananabomb.codegen.writer.JavaFile;

/**
 * Created by aw on 2017-04-22.
 */

public class TemplaterImpl implements Templater {

    @Override
    public JavaFile render(Template template, Object model) {

        MustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache mustache = mustacheFactory.compile(new StringReader(template.getTemplate()), template.getTypeName().toString());

        StringWriter writer = new StringWriter();
        mustache.execute(writer, model);
        String formatted = writer.toString();

        return new JavaFile(template.getTypeName(), formatted);
    }
}
