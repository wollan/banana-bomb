package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.lang.reflect.Method;
import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.PreviousResults;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-02.
 */

class DetonateBananaExtractor implements BananaExtractor {

    private final Elements elements;

    public DetonateBananaExtractor(Elements elements) {
        this.elements = elements;
    }

    @Override
    public ImmutableSet<Banana> extract(BananaBomb bananaBomb, PreviousResults previousResults) {

        Iterable<ExecutableElement> methods =
                ElementFilter.methodsIn(elements.getAllMembers(bananaBomb.getTypeElement()));

        ImmutableSet.Builder<Banana> bananaBuilder = ImmutableSet.builder();
        for (ExecutableElement method : methods) {
            if (!method.getSimpleName().toString().equals("detonate"))
                continue;

            List<? extends VariableElement> params = method.getParameters();
            if (params.size() != 1)
                throw new IllegalArgumentException("detonator method "+method+" in "+bananaBomb+
                    " must have one parameter");

            String firstParamTypeName = params.get(0).asType().toString();
            bananaBuilder.add(new Banana(toCanonical(firstParamTypeName)));
        }

        return bananaBuilder.build();
    }
}
