package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;

import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.util.SuperTyper;

import static se.wollan.bananabomb.codegen.processors.ProcessInjector.injectInto;

public class BananaTargetProcessor extends AbstractProcessor {

    private ProcessingCoordinator coordinator;
    private SuperTyper superTyper;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        injectInto(this, processingEnvironment);
    }

    void inject(ProcessingCoordinator coordinator, SuperTyper superTyper) {
        this.coordinator = coordinator;
        this.superTyper = superTyper;
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment env) {
        ImmutableSet<BananaTarget> bananaTargets = BananaTarget.getAnnotatedTargets(superTyper, env);
        coordinator.coordinateBananaTargets(bananaTargets);
        return false;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_7;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return ImmutableSet.of(se.wollan.bananabomb.BananaTarget.class.getCanonicalName());
    }

}
