package se.wollan.bananabomb.codegen.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by aw on 2017-06-10.
 */

public class DateFormats {

    private static final DateFormat ISO6801;

    static {
        ISO6801 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        ISO6801.setTimeZone(TimeZone.getDefault());
    }

    public static String nowAsISO8601() {
        return ISO6801.format(new Date());
    }
}
