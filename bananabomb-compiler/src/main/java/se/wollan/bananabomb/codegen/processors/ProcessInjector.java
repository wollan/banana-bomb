package se.wollan.bananabomb.codegen.processors;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;

import se.wollan.bananabomb.codegen.bootstrapping.Module;
import se.wollan.bananabomb.codegen.util.SuperTyperImpl;

/**
 * Created by aw on 2017-04-23.
 */

class ProcessInjector {

    private static ProcessingCoordinator COORDINATOR;

    static void injectInto(BananaProcessor processor, ProcessingEnvironment env) {
        processor.inject(getCoordinator(env), new SuperTyperImpl(env.getTypeUtils()));
    }

    static void injectInto(BananaTargetProcessor processor, ProcessingEnvironment env) {
        processor.inject(getCoordinator(env), new SuperTyperImpl(env.getTypeUtils()));
    }

    public static void injectInto(BananaBombProcessor processor, ProcessingEnvironment processingEnvironment) {
        processor.inject(getCoordinator(processingEnvironment));
    }

    private static ProcessingCoordinator getCoordinator(ProcessingEnvironment env) {
        if (COORDINATOR == null) {

            List<BombFactory> bombFactories = new LinkedList<>();
            for (Module module : Module.MODULES) {
                bombFactories.add(module.createFactory(env));
            }

            COORDINATOR = new ProcessingCoordinatorImpl(bombFactories, env.getMessager());
        }
        return COORDINATOR;
    }

}
