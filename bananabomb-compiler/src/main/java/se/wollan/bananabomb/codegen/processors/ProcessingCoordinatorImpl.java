package se.wollan.bananabomb.codegen.processors;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import javax.annotation.processing.Messager;
import javax.tools.Diagnostic;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.emptyIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;

class ProcessingCoordinatorImpl implements ProcessingCoordinator {

    private final Iterable<BombFactory> bombFactories;
    private final Messager messager;
    private ImmutableSet<Banana> bananas;
    private ImmutableSet<BananaTarget> bananaTargets;
    private ImmutableSet<BananaBomb> bananaBombs;

    ProcessingCoordinatorImpl(Iterable<BombFactory> bombFactories, Messager messager) {
        this.bombFactories = bombFactories;
        this.messager = messager;
    }

    @Override
    public void coordinateBananas(ImmutableSet<Banana> bananas) {
        throwIfNull(bananas, "bananas");
        if (this.bananas == null) {
            this.bananas = bananas;
            startFactoring();
        }
    }

    @Override
    public void coordinateBananaTargets(ImmutableSet<BananaTarget> bananaTargets) {
        throwIfNull(bananaTargets, "bananaTargets");
        if (this.bananaTargets == null) {
            this.bananaTargets = BananaTarget.enrichBananas(bananaTargets, bananas);
            startFactoring();
        }
    }

    @Override
    public void coordinateBananaBombs(ImmutableSet<BananaBomb> bananaBombs) {
        throwIfNull(bananaBombs, "bananaBombs");
        if (this.bananaBombs == null) {
            this.bananaBombs = bananaBombs;
            startFactoring();
        }
    }

    private void startFactoring(){
        try {
            PreviousResults previousResults = PreviousResults.empty();

            for (BombFactory factory : bombFactories) {
                FactorResult result =
                        factory.factor(emptyIfNull(bananas),
                                emptyIfNull(bananaTargets),
                                emptyIfNull(bananaBombs),
                                previousResults);

                previousResults = previousResults.append(result);
            }
        } catch (Throwable e) {
            Throwable realError = Throwables.getRootCause(e);
            String stackTraceAsString = Throwables.getStackTraceAsString(realError);
            messager.printMessage(Diagnostic.Kind.ERROR,
                    "BananaBomb: Failed to process annotations: " + stackTraceAsString);
        }
    }


}
