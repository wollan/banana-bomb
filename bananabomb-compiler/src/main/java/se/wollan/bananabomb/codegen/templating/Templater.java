package se.wollan.bananabomb.codegen.templating;


import se.wollan.bananabomb.codegen.writer.JavaFile;

/**
 * Created by aw on 2017-04-22.
 */
public interface Templater {

    JavaFile render(Template template, Object model);

}

