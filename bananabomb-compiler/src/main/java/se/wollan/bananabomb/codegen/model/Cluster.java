package se.wollan.bananabomb.codegen.model;

import java.lang.annotation.Annotation;

import se.wollan.bananabomb.*;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;

/**
 * Create clusters of bananas by extending @Bananas annotation.
 */
public final class Cluster {

    public static final Cluster TOP_LEVEL = toCluster(se.wollan.bananabomb.Banana.class);

    private final CanonicalName canonicalName;

    public Cluster(CanonicalName canonicalName) {
        this.canonicalName = throwIfNull(canonicalName, "canonicalName");
    }

    public static Cluster toCluster(Class<? extends Annotation> annotation) {
        return new Cluster(toCanonical(annotation));
    }

    public static Cluster toCluster(CanonicalName canonicalName) {
        return new Cluster(canonicalName);
    }

    public static Cluster toCluster(String canonicalName) {
        return new Cluster(toCanonical(canonicalName));
    }

    public CanonicalName getCanonical() {
        return canonicalName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cluster cluster = (Cluster) o;
        return canonicalName.equals(cluster.canonicalName);
    }

    @Override
    public int hashCode() {
        return canonicalName.hashCode();
    }

    @Override
    public String toString() {
        return canonicalName.toString();
    }

    public boolean isTopLevel() {
        return equals(TOP_LEVEL);
    }
}
