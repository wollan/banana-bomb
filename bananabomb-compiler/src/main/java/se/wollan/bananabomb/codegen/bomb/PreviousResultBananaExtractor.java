package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.GeneratedFileResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;
import se.wollan.bananabomb.codegen.util.SuperTyper;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-07.
 */

class PreviousResultBananaExtractor implements BananaExtractor {

    private final SuperTyper superTyper;

    PreviousResultBananaExtractor(SuperTyper superTyper) {
        this.superTyper = superTyper;
    }

    @Override
    public ImmutableSet<Banana> extract(BananaBomb bananaBomb, PreviousResults previousResults) {

        ImmutableSet<CanonicalName> supertypes = superTyper.getAllSupertypes(bananaBomb.getTypeElement().asType());

        ImmutableSet.Builder<Banana> bananaBuilder = ImmutableSet.builder();

        for (CanonicalName supertype : supertypes) {

            GeneratedFileResult result = previousResults.getResultByGeneratedFileNameOrNull(supertype);
            if (result == null) {
                continue;
            }

            bananaBuilder.addAll(result.getUsedBananas());
        }

        return bananaBuilder.build();
    }

}
