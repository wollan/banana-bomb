package se.wollan.bananabomb.codegen.util;

import com.google.common.collect.ImmutableSet;

import java.lang.annotation.Annotation;

import javax.lang.model.element.Element;

import se.wollan.bananabomb.codegen.model.CanonicalName;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrEmpty;

public class ElementAndAnnotations {
    private final Element element;
    private final ImmutableSet<CanonicalName> annotations;

    public ElementAndAnnotations(Element element, ImmutableSet<CanonicalName> annotations) {
        this.element = throwIfNull(element, "element");
        this.annotations = throwIfNullOrEmpty(annotations, "annotations");
    }

    public Element getElement() {
        return element;
    }

    public ImmutableSet<CanonicalName> getAnnotations() {
        return annotations;
    }
}
