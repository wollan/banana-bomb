package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.CanonicalName;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;

public class GeneratedFileResult {
    private final ImmutableSet<Banana> usedBananas;
    private final ImmutableSet<BananaTarget> usedBananaTargets;
    private final ImmutableSet<BombingRange> usedBombingRanges;
    private final ImmutableSet<BananaBomb> usedBananaBombs;
    private final CanonicalName canonicalNameOfFile;

    public GeneratedFileResult(ImmutableSet<Banana> usedBananas,
                               ImmutableSet<BananaTarget> usedBananaTargets,
                               ImmutableSet<BombingRange> usedBombingRanges,
                               ImmutableSet<BananaBomb> usedBananaBombs,
                               CanonicalName canonicalNameOfFile) {
        this.usedBananas = throwIfNull(usedBananas, "usedBananas");
        this.usedBananaTargets = throwIfNull(usedBananaTargets, "usedBananaTargets");
        this.usedBombingRanges = throwIfNull(usedBombingRanges, "usedBombingRanges");
        this.usedBananaBombs = throwIfNull(usedBananaBombs, "usedBananaBombs");
        this.canonicalNameOfFile = throwIfNull(canonicalNameOfFile, "canonicalNameOfFile");
    }

    public static GeneratedFileResult fromBanana(Banana usedBanana, CanonicalName canonicalNameOfFile) {
        return new GeneratedFileResult(
                ImmutableSet.of(throwIfNull(usedBanana, "usedBanana")),
                ImmutableSet.<BananaTarget>of(),
                ImmutableSet.<BombingRange>of(),
                ImmutableSet.<BananaBomb>of(),
                throwIfNull(canonicalNameOfFile, "canonicalNameOfFile"));
    }

    public static GeneratedFileResult fromBananasAndBomb(ImmutableSet<Banana> usedBananas,
                                                         BananaBomb usedBananaBomb,
                                                         CanonicalName canonicalNameOfFile) {
        return new GeneratedFileResult(
                throwIfNull(usedBananas, "usedBananas"),
                ImmutableSet.<BananaTarget>of(),
                ImmutableSet.<BombingRange>of(),
                ImmutableSet.of(throwIfNull(usedBananaBomb, "usedBananaBomb")),
                throwIfNull(canonicalNameOfFile, "canonicalNameOfFile"));
    }

    public static GeneratedFileResult fromBananas(ImmutableSet<Banana> usedBananas, CanonicalName canonicalNameOfFile) {
        return new GeneratedFileResult(
                throwIfNull(usedBananas, "usedBananas"),
                ImmutableSet.<BananaTarget>of(),
                ImmutableSet.<BombingRange>of(),
                ImmutableSet.<BananaBomb>of(),
                throwIfNull(canonicalNameOfFile, "canonicalNameOfFile"));
    }

    public static GeneratedFileResult fromTargetsAndRange(ImmutableSet<BananaTarget> usedBananaTargets,
                                                          BombingRange usedBombingRange,
                                                          CanonicalName canonicalNameOfFile) {
        return new GeneratedFileResult(
                ImmutableSet.<Banana>of(),
                throwIfNull(usedBananaTargets, "usedBananaTargets"),
                ImmutableSet.of(throwIfNull(usedBombingRange, "usedBombingRange")),
                ImmutableSet.<BananaBomb>of(),
                throwIfNull(canonicalNameOfFile, "canonicalNameOfFile"));
    }

    public static GeneratedFileResult fromRange(BombingRange usedBombingRange, CanonicalName canonicalNameOfFile) {
        return new GeneratedFileResult(
                ImmutableSet.<Banana>of(),
                ImmutableSet.<BananaTarget>of(),
                ImmutableSet.of(throwIfNull(usedBombingRange, "usedBombingRange")),
                ImmutableSet.<BananaBomb>of(),
                throwIfNull(canonicalNameOfFile, "canonicalNameOfFile"));
    }

    public ImmutableSet<Banana> getUsedBananas() {
        return usedBananas;
    }

    public ImmutableSet<BananaTarget> getUsedBananaTargets() {
        return usedBananaTargets;
    }

    public ImmutableSet<BombingRange> getUsedBombingRanges() {
        return usedBombingRanges;
    }

    public ImmutableSet<BananaBomb> getUsedBananaBombs() {
        return usedBananaBombs;
    }

    public CanonicalName getCanonicalNameOfFile() {
        return canonicalNameOfFile;
    }
}
