package se.wollan.bananabomb.codegen.detonator;

import javax.annotation.processing.ProcessingEnvironment;

import se.wollan.bananabomb.codegen.bootstrapping.Module;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.templating.Template;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.templating.TemplaterImpl;
import se.wollan.bananabomb.codegen.util.ResourceLoaderImpl;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;
import se.wollan.bananabomb.codegen.writer.JavaFileWriterImpl;

/**
 * Created by aw on 2017-04-22.
 */

public class DetonatorModule implements Module {

    @Override
    public BombFactory createFactory(ProcessingEnvironment env) {

        Templater templater = new TemplaterImpl();
        JavaFileWriter writer = new JavaFileWriterImpl(env.getFiler());
        return new DetonatorFactory(templater, new ResourceLoaderImpl(), writer);
    }
}
