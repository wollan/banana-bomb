package se.wollan.bananabomb.codegen.processors;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.util.SuperAnnotatorImpl;
import se.wollan.bananabomb.codegen.util.SuperTyper;

import static se.wollan.bananabomb.codegen.processors.ProcessInjector.injectInto;

public class BananaProcessor extends AbstractProcessor {

    private ProcessingCoordinator coordinator;
    private SuperTyper superTyper;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        injectInto(this, processingEnvironment);
    }

    void inject(ProcessingCoordinator coordinator, SuperTyper superTyper) {
        this.coordinator = coordinator;
        this.superTyper = superTyper;
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment env) {
        ImmutableSet<Banana> bananas = Banana.getAnnotatedBananas(superTyper, new SuperAnnotatorImpl(env));
        coordinator.coordinateBananas(bananas);
        return true;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_7;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return ImmutableSet.of(se.wollan.bananabomb.Banana.class.getCanonicalName());
    }

}
