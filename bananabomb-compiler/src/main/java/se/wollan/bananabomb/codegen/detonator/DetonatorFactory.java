package se.wollan.bananabomb.codegen.detonator;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.model.Cluster;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.processors.FactorResult;
import se.wollan.bananabomb.codegen.processors.GeneratedFileResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;
import se.wollan.bananabomb.codegen.templating.Template;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.util.ResourceLoader;
import se.wollan.bananabomb.codegen.writer.JavaFile;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;

import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * Created by aw on 2017-04-22.
 */

class DetonatorFactory implements BombFactory {

    private final Templater templater;
    private final ResourceLoader resourceLoader;
    private final JavaFileWriter fileWriter;

    DetonatorFactory(Templater templater, ResourceLoader resourceLoader,
                     JavaFileWriter fileWriter) {
        this.templater = templater;
        this.resourceLoader = resourceLoader;
        this.fileWriter = fileWriter;
    }

    @Override
    public FactorResult factor(ImmutableSet<Banana> bananas, ImmutableSet<BananaTarget> bananaTargets,
                               ImmutableSet<BananaBomb> bananaBombs, PreviousResults previousResults) throws IOException {
        FactorResult result = FactorResult.create("DetonatorFactory");

        for (Banana banana : bananas) {
            result = result.append(factorDetonator(banana));
        }

        if (bananaBombs.size() > 0) {
            BananaBomb bananaBomb = BananaBomb.mostTopLevelPackage(bananaBombs);
            result = result.append(factorCommonDetonator(bananas, bananaBomb));
        }

        //group by cluster
        Set<Cluster> collect = bananas.stream().flatMap(b -> b.getBelongsTo().stream()).collect(toSet());
        for (Cluster cluster : collect) {
            if (!cluster.isTopLevel()) {
                result = result.append(factorClusterDetonator(
                        cluster,
                        bananas.stream().filter(b -> b.belongsTo(cluster)).collect(toImmutableSet())));
            }
        }

        return result;
    }

    private GeneratedFileResult factorDetonator(Banana banana) throws IOException {
        DetonatorTemplateModel templateModel = new DetonatorTemplateModel(banana);

        String templateData = resourceLoader.loadResourceDataByName("detonator.mustache");
        Template template = new Template(templateData, templateModel.interfaceName);
        renderAndWrite(templateModel, template);

        return GeneratedFileResult.fromBanana(banana, templateModel.interfaceName);
    }

    private GeneratedFileResult factorCommonDetonator(ImmutableSet<Banana> bananas, BananaBomb bananaBomb) throws IOException {
        CommonDetonatorTemplateModel templateModel = new CommonDetonatorTemplateModel(bananaBomb, bananas);
        String templateData = resourceLoader.loadResourceDataByName("commondetonator.mustache");
        Template template = new Template(templateData, templateModel.interfaceName);
        renderAndWrite(templateModel, template);
        return GeneratedFileResult.fromBananasAndBomb(bananas, bananaBomb, templateModel.interfaceName);
    }

    private GeneratedFileResult factorClusterDetonator(Cluster cluster, ImmutableSet<Banana> bananas) throws IOException {
        ClusterDetonatorTemplateModel templateModel = new ClusterDetonatorTemplateModel(cluster, bananas);
        String templateData = resourceLoader.loadResourceDataByName("clusterdetonator.mustache");
        Template template = new Template(templateData, templateModel.interfaceName);
        renderAndWrite(templateModel, template);
        return GeneratedFileResult.fromBananas(bananas, templateModel.interfaceName);
    }

    private void renderAndWrite(Object templateModel, Template template) throws IOException {
        JavaFile javaFile = templater.render(template, templateModel);
        fileWriter.write(javaFile);
    }
}
