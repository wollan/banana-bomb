package se.wollan.bananabomb.codegen.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;

import se.wollan.bananabomb.codegen.model.CanonicalName;

import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static java.util.stream.Collectors.toList;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;

/**
 * Created by aw on 2017-06-11.
 */

public class SuperAnnotatorImpl implements SuperAnnotator {

    private final RoundEnvironment roundEnvironment;

    public SuperAnnotatorImpl(RoundEnvironment roundEnvironment) {
        this.roundEnvironment = throwIfNull(roundEnvironment, "roundEnvironment");
    }

    @Override
    public ImmutableSet<ElementAndAnnotations> getElementsAnnotatedWithIncludingSubAnnotations(Class<? extends Annotation> annotation) {

        List<ElementAnnotationPair> pairs = getAnnotatedElems(annotation);

        Map<Element,List<ElementAnnotationPair>> groupedByElement = pairs.stream()
                .collect(Collectors.groupingBy(ElementAnnotationPair::getElement));

        ImmutableSet.Builder<ElementAndAnnotations> elemsAndAnnotations = ImmutableSet.builder();
        for (Map.Entry<Element, List<ElementAnnotationPair>> pair : groupedByElement.entrySet()) {
            ImmutableSet<CanonicalName> annotations = pair.getValue().stream()
                    .map(p -> p.annotation)
                    .collect(toImmutableSet());

            elemsAndAnnotations.add(new ElementAndAnnotations(pair.getKey(), annotations));
        }
        return elemsAndAnnotations.build();
    }

    private List<ElementAnnotationPair> getAnnotatedElems(Class<? extends Annotation> annotation) {
        Set<? extends Element> elemsInclSubAnnotations = roundEnvironment.getElementsAnnotatedWith(annotation);
        return getAnnotatedElems(ImmutableList.of(toCanonical(annotation)), elemsInclSubAnnotations);
    }

    private List<ElementAnnotationPair> getAnnotatedElems(ImmutableList<CanonicalName> annotationHierarchy,
                                                          TypeElement annotation) {
        Set<? extends Element> elemsInclSubAnnotations = roundEnvironment.getElementsAnnotatedWith(annotation);
        return getAnnotatedElems(
                ImmutableUtils.concat(annotationHierarchy, toCanonical(annotation.toString())), //append to hierarchy
                elemsInclSubAnnotations);
    }

    private List<ElementAnnotationPair> getAnnotatedElems(ImmutableList<CanonicalName> annotationHierarchy,
                                                          Set<? extends Element> elemsInclSubAnnotations) {

        List<ElementAnnotationPair> annotatedElems = new ArrayList<>();

        for (Element elem : elemsInclSubAnnotations) {
            if (elem.getKind() == ElementKind.ANNOTATION_TYPE) {
                List<ElementAnnotationPair> annotatedElemsOneLevelDown = getAnnotatedElems(annotationHierarchy, (TypeElement) elem);
                annotatedElems.addAll(annotatedElemsOneLevelDown);
            } else {
                annotatedElems.addAll(fromHierarchy(elem, annotationHierarchy));
            }
        }
        return annotatedElems;
    }

    private static class ElementAnnotationPair {
        private final Element element;
        private final CanonicalName annotation;

        private ElementAnnotationPair(Element element, CanonicalName annotation) {
            this.element = throwIfNull(element, "element");
            this.annotation = throwIfNull(annotation, "annotation");
        }

        Element getElement() {
            return element;
        }

        CanonicalName getAnnotation() {
            return annotation;
        }
    }

    private static Collection<ElementAnnotationPair> fromHierarchy(Element element, Collection<CanonicalName> annotationHierarchy) {
        return annotationHierarchy.stream()
                .map(a -> new ElementAnnotationPair(element, a))
                .collect(toList());
    }
}
