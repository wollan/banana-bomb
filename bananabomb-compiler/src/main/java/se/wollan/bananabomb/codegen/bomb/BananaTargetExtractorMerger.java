package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.processors.PreviousResults;

public class BananaTargetExtractorMerger implements BananaTargetExtractor {

    private final Iterable<BananaTargetExtractor> bananaExtractors;

    public BananaTargetExtractorMerger(Iterable<BananaTargetExtractor> bananaExtractors) {
        this.bananaExtractors = bananaExtractors;
    }

    @Override
    public ImmutableSet<BananaTarget> extract(BananaBomb bananaBomb, PreviousResults previousResults) {
        ImmutableSet.Builder<BananaTarget> bananaBuilder = ImmutableSet.builder();
        for (BananaTargetExtractor extractor : bananaExtractors) {
            bananaBuilder.addAll(extractor.extract(bananaBomb, previousResults));
        }
        return bananaBuilder.build();
    }
}
