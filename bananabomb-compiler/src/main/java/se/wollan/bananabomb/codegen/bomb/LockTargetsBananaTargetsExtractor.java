package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.lang.reflect.Method;
import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.PreviousResults;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-02.
 */

class LockTargetsBananaTargetsExtractor implements BananaTargetExtractor {

    private final Elements elements;
    private final Types types;

    public LockTargetsBananaTargetsExtractor(Elements elements, Types types) {
        this.elements = elements;
        this.types = types;
    }

    @Override
    public ImmutableSet<BananaTarget> extract(BananaBomb bananaBomb, PreviousResults previousResults) {

        Iterable<ExecutableElement> methods =
                ElementFilter.methodsIn(elements.getAllMembers(bananaBomb.getTypeElement()));

        ImmutableSet.Builder<BananaTarget> bananaTargetBuilder = ImmutableSet.builder();
        for (ExecutableElement radarMethod : methods) {
            if (!radarMethod.getSimpleName().toString().equals("lockTargets"))
                continue;

            List<? extends VariableElement> lockTargetParams = radarMethod.getParameters();
            if (lockTargetParams.size() == 2)
                continue; //there's one overloaded method, lets ignore that one
            if (lockTargetParams.size() != 1)
                throw new IllegalArgumentException("lockTargets radarMethod "+radarMethod+" in class "
                        + bananaBomb + " must have one or two parameters");

            VariableElement bombingRangeVarElement = lockTargetParams.get(0);
            TypeElement bombingRangeTypeElement = (TypeElement)types.asElement(bombingRangeVarElement.asType());
            Iterable<ExecutableElement> bombingRangeMethods =
                    ElementFilter.methodsIn(elements.getAllMembers((TypeElement) bombingRangeTypeElement));
            for(ExecutableElement bananaTargetMethod : bombingRangeMethods) {
                if (bananaTargetMethod.getAnnotation(se.wollan.bananabomb.BananaTarget.class) == null)
                    continue;

                List<? extends VariableElement> bananaTargetParams = bananaTargetMethod.getParameters();
                if (bananaTargetParams.size() != 1)
                    throw new IllegalArgumentException("Banana target method " + bananaTargetMethod +
                            " must have one parameter");

                String firstParamType = bananaTargetParams.get(0).asType().toString();
                Banana banana = new Banana(toCanonical(firstParamType));
                BombingRange bombingRange = new BombingRange(bombingRangeTypeElement);
                BananaTarget bananaTarget = new BananaTarget(bombingRange,
                        bananaTargetMethod.getSimpleName().toString(), banana);
                bananaTargetBuilder.add(bananaTarget);
            }
        }

        return bananaTargetBuilder.build();
    }
}
