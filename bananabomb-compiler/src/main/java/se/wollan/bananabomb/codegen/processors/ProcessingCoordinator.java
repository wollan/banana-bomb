package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;

/**
 * Created by aw on 2017-04-24.
 */

interface ProcessingCoordinator {

    void coordinateBananas(ImmutableSet<Banana> bananas);

    void coordinateBananaTargets(ImmutableSet<BananaTarget> bananaTargets);

    void coordinateBananaBombs(ImmutableSet<BananaBomb> bananaBombs);
}
