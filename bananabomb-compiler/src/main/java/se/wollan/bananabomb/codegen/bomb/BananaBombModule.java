package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import se.wollan.bananabomb.codegen.bootstrapping.Module;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.templating.TemplaterImpl;
import se.wollan.bananabomb.codegen.util.ResourceLoader;
import se.wollan.bananabomb.codegen.util.ResourceLoaderImpl;
import se.wollan.bananabomb.codegen.util.SuperTyper;
import se.wollan.bananabomb.codegen.util.SuperTyperImpl;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;
import se.wollan.bananabomb.codegen.writer.JavaFileWriterImpl;

/**
 * Created by aw on 2017-04-24.
 */

public class BananaBombModule implements Module {


    @Override
    public BombFactory createFactory(ProcessingEnvironment env) {

        Templater templater = new TemplaterImpl();

        JavaFileWriter writer = new JavaFileWriterImpl(env.getFiler());
        ResourceLoader resourceLoader = new ResourceLoaderImpl();

        Elements elementUtils = env.getElementUtils();
        Types types = env.getTypeUtils();
        SuperTyper superTyper = new SuperTyperImpl(types);

        //extractors
        BananaExtractorMerger bananaExtractorMerger = createBananaExtractorMerger(elementUtils, superTyper);
        BananaTargetExtractorMerger bananaTargetExtractorMerger = createBananaTargetExtractorMerger(elementUtils, types, superTyper);

        return new BananaBombFactory(templater, bananaExtractorMerger,
                bananaTargetExtractorMerger, writer, resourceLoader);
    }

    private BananaExtractorMerger createBananaExtractorMerger(Elements elementUtils, SuperTyper superTyper) {
        DetonateBananaExtractor detonateBananaExtractor = new DetonateBananaExtractor(elementUtils);
        PreviousResultBananaExtractor previousResultBananaExtractor = new PreviousResultBananaExtractor(superTyper);
        return new BananaExtractorMerger(ImmutableList.of(detonateBananaExtractor, previousResultBananaExtractor));
    }

    private BananaTargetExtractorMerger createBananaTargetExtractorMerger(Elements elementUtils, Types types, SuperTyper superTyper) {
        LockTargetsBananaTargetsExtractor lockTargetsBananaTargetsExtractor =
                new LockTargetsBananaTargetsExtractor(elementUtils, types);
        PreviousResultBananaTargetExtractor previousResultBananaTargetExtractor =
                new PreviousResultBananaTargetExtractor(superTyper);
        return new BananaTargetExtractorMerger(ImmutableList.of(lockTargetsBananaTargetsExtractor, previousResultBananaTargetExtractor));
    }
}
