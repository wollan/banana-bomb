package se.wollan.bananabomb.codegen.radar;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.model.Cluster;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.processors.FactorResult;
import se.wollan.bananabomb.codegen.processors.GeneratedFileResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;
import se.wollan.bananabomb.codegen.templating.Template;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.util.ResourceLoader;
import se.wollan.bananabomb.codegen.writer.JavaFile;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static se.wollan.bananabomb.codegen.model.BananaTarget.uniqueBombingRanges;

/**
 * Created by aw on 2017-04-23.
 */

class RadarFactory implements BombFactory {

    private final Templater templater;
    private final ResourceLoader resourceLoader;
    private final JavaFileWriter fileWriter;

    RadarFactory(Templater templater, ResourceLoader resourceLoader,
                 JavaFileWriter fileWriter) {
        this.templater = templater;
        this.resourceLoader = resourceLoader;
        this.fileWriter = fileWriter;
    }

    @Override
    public FactorResult factor(ImmutableSet<Banana> bananas, ImmutableSet<BananaTarget> allBananaTargets,
                               ImmutableSet<BananaBomb> bananaBombs, PreviousResults previousResults) throws IOException {
        FactorResult result = FactorResult.create("RadarFactory");

        ImmutableSet<BombingRange> bombingRanges = uniqueBombingRanges(allBananaTargets);

        for(BombingRange bombingRange : bombingRanges) {
            ImmutableSet<BananaTarget> targetsInRange = BananaTarget.targetsWithinRange(allBananaTargets, bombingRange);
            result = result.append(factorRadar(bombingRange, targetsInRange));
        }

        if (bananaBombs.size() > 0) {
            BananaBomb bananaBomb = BananaBomb.mostTopLevelPackage(bananaBombs);
            result = result.append(factorCommonRadar(bombingRanges, bananaBomb, allBananaTargets));
        }

        //group by cluster
        ImmutableSet<Cluster> clusters = allBananaTargets.stream()
                .flatMap(t -> t.getBanana().getBelongsTo().stream())
                .filter(c -> !c.isTopLevel())
                .collect(toImmutableSet());

        for (Cluster cluster : clusters) {
            result = result.append(factorClusterRadar(cluster, allBananaTargets.stream()
                    .filter(t -> t.getBanana().belongsTo(cluster))
                    .collect(toImmutableSet())));
        }

        return result;
    }

    private GeneratedFileResult factorRadar(BombingRange bombingRange, ImmutableSet<BananaTarget> targetsInRange) throws IOException {
        RadarTemplateModel templateModel = new RadarTemplateModel(bombingRange, targetsInRange);

        String templateData = resourceLoader.loadResourceDataByName("radar.mustache");
        Template template = new Template(templateData, templateModel.interfaceName);
        renderAndWrite(templateModel, template);

        return GeneratedFileResult.fromTargetsAndRange(targetsInRange, bombingRange, templateModel.interfaceName);
    }

    private GeneratedFileResult factorCommonRadar(ImmutableSet<BombingRange> bombingRanges,
                                                  BananaBomb bananaBomb,
                                                  ImmutableSet<BananaTarget> bananaTargets) throws IOException {
        CommonRadarTemplateModel templateModel = new CommonRadarTemplateModel(bananaBomb, bombingRanges);
        String templateData = resourceLoader.loadResourceDataByName("commonradar.mustache");
        Template template = new Template(templateData, templateModel.interfaceName);
        renderAndWrite(templateModel, template);
        return new GeneratedFileResult(ImmutableSet.of(), bananaTargets, bombingRanges,
                ImmutableSet.of(bananaBomb), templateModel.interfaceName);
    }

    private GeneratedFileResult factorClusterRadar(Cluster cluster, ImmutableSet<BananaTarget> bananaTargets) throws IOException {
        ImmutableSet<BombingRange> bombingRanges = bananaTargets.stream()
                .map(BananaTarget::getBombingRange)
                .collect(toImmutableSet());
        ClusterRadarTemplateModel templateModel = new ClusterRadarTemplateModel(cluster, bombingRanges);
        String templateData = resourceLoader.loadResourceDataByName("clusterradar.mustache");
        Template template = new Template(templateData, templateModel.interfaceName);
        renderAndWrite(templateModel, template);
        return new GeneratedFileResult(ImmutableSet.of(), bananaTargets, bombingRanges,
                ImmutableSet.of(), templateModel.interfaceName);
    }

    private void renderAndWrite(Object templateModel, Template template) throws IOException {
        JavaFile javaFile = templater.render(template, templateModel);
        fileWriter.write(javaFile);
    }
}
