package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.io.IOException;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.processors.FactorResult;
import se.wollan.bananabomb.codegen.processors.GeneratedFileResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;
import se.wollan.bananabomb.codegen.templating.Template;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.util.ResourceLoader;
import se.wollan.bananabomb.codegen.writer.JavaFile;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;

import static se.wollan.bananabomb.codegen.model.BananaTarget.hasRottenTarget;
import static se.wollan.bananabomb.codegen.util.ImmutableUtils.concat;

public class BananaBombFactory implements BombFactory {

    private final Templater templater;
    private final BananaExtractor bananaExtractor;
    private final BananaTargetExtractor bananaTargetExtractor;
    private final JavaFileWriter fileWriter;
    private final ResourceLoader resourceLoader;

    BananaBombFactory(Templater templater, BananaExtractor bananaExtractor, BananaTargetExtractor bananaTargetExtractor,
                      JavaFileWriter fileWriter, ResourceLoader resourceLoader) {
        this.templater = templater;
        this.bananaExtractor = bananaExtractor;
        this.bananaTargetExtractor = bananaTargetExtractor;
        this.fileWriter = fileWriter;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public FactorResult factor(ImmutableSet<Banana> bananas, ImmutableSet<BananaTarget> bananaTargets,
                               ImmutableSet<BananaBomb> bananaBombs, PreviousResults previousResults) throws IOException {
        FactorResult result = FactorResult.create("BananaBombFactory");
        for (BananaBomb bananaBomb : bananaBombs) {
            result = result.append(factor(bananaBomb, previousResults));
        }
        return result;
    }

    private GeneratedFileResult factor(BananaBomb bananaBomb, PreviousResults previousResults) throws IOException {

        BananaBombTemplateModel templateModel = createTemplateModel(bananaBomb, previousResults);
        String templateData  = resourceLoader.loadResourceDataByName("bananabomb.mustache");
        Template template  = new Template(templateData, templateModel.className);
        JavaFile javaFile = templater.render(template, templateModel);
        fileWriter.write(javaFile);

        return new GeneratedFileResult(
                templateModel.bananas,
                templateModel.targets,
                ImmutableSet.<BombingRange>of(),
                ImmutableSet.of(bananaBomb), templateModel.className);
    }

    private BananaBombTemplateModel createTemplateModel(BananaBomb bananaBomb, PreviousResults previousResults) {
        ImmutableSet<Banana> extractedBananas = bananaExtractor.extract(bananaBomb, previousResults);
        ImmutableSet<BananaTarget> extractedTargets = bananaTargetExtractor.extract(bananaBomb, previousResults);

        return new BananaBombTemplateModel(bananaBomb, extractedBananas, extractedTargets);
    }

}
