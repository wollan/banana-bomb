package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.BananaBombProcessor;
import se.wollan.bananabomb.codegen.util.DateFormats;

import static se.wollan.bananabomb.codegen.model.Banana.hasExceptionalBanana;
import static se.wollan.bananabomb.codegen.model.Banana.hasRottenBanana;
import static se.wollan.bananabomb.codegen.model.BananaTarget.getBombingRangesMatchingBanana;
import static se.wollan.bananabomb.codegen.model.BananaTarget.hasExceptionalTarget;
import static se.wollan.bananabomb.codegen.model.BananaTarget.hasRottenTarget;
import static se.wollan.bananabomb.codegen.model.BananaTarget.uniqueBombingRanges;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.DateFormats.nowAsISO8601;
import static se.wollan.bananabomb.codegen.util.ImmutableUtils.concat;

/**
 * Created by aw on 2017-04-23.
 */

class BananaBombTemplateModel {

    final CanonicalName processor = toCanonical(BananaBombProcessor.class);
    final String date = nowAsISO8601();
    final CanonicalName className;
    final BananaBomb bananaBomb;
    final ImmutableSet<DetonateMethodModel> detonateMethods;
    final DetonateMethodModel detonateRottenBanana;
    final DetonateMethodModel detonateExceptionalBanana;
    final ImmutableSet<Banana> bananas;
    final ImmutableSet<BananaTarget> targets;
    final ImmutableSet<BombingRange> ranges;

    BananaBombTemplateModel(BananaBomb bananaBomb, ImmutableSet<Banana> bananas,
                            ImmutableSet<BananaTarget> bananaTargets) {
        if (hasRottenBanana(bananas)) {
            throw new IllegalArgumentException("Banana bombs mustn't detonate rotten bananas, " +
                "please remove detonate(RottenBanana) method from "+bananaBomb+".");
        }
        if (hasExceptionalBanana(bananas)) {
            throw new IllegalArgumentException("Banana bombs mustn't detonate exceptional bananas, " +
                "please remove detonate(ExceptionalBanana) method from "+bananaBomb+".");
        }

        if (hasRottenTarget(bananaTargets)) {
            bananas = concat(bananas, Banana.ROTTEN);
        }
        if (hasExceptionalTarget(bananaTargets)) {
            bananas = concat(bananas, Banana.EXCEPTIONAL);
        }

        checkIfAllBananasHaveATarget(bananaBomb, bananas, bananaTargets);
        checkIfAllTargetsHaveABanana(bananaBomb, bananas, bananaTargets);

        ImmutableSet.Builder<DetonateMethodModel> detonateMethodBuilder = ImmutableSet.builder();
        DetonateMethodModel detonateRottenBananaModel = null;
        DetonateMethodModel detonateExceptionalBanana = null;
        for (Banana banana : bananas) {
            DetonateMethodModel detonateMethodModel = new DetonateMethodModel(banana, bananaTargets);
            if (banana.isRotten()) {
                detonateRottenBananaModel = detonateMethodModel;
            } else if (banana.isExceptional()) {
                detonateExceptionalBanana = detonateMethodModel;
            } else {
                detonateMethodBuilder.add(detonateMethodModel);
            }
        }

        this.className = bananaBomb.getCanonical().withSuffix("Impl");
        this.bananaBomb = bananaBomb;
        this.detonateMethods = detonateMethodBuilder.build();
        this.detonateRottenBanana = detonateRottenBananaModel;
        this.detonateExceptionalBanana = detonateExceptionalBanana;
        this.bananas = bananas;
        this.targets = bananaTargets;
        this.ranges = uniqueBombingRanges(bananaTargets);
    }

    static class DetonateMethodModel {
        final Banana banana;
        final ImmutableSet<BombingRange> ranges;

        DetonateMethodModel(Banana banana, ImmutableSet<BananaTarget> allBananaTargets) {
            this.banana = banana;
            ranges = getBombingRangesMatchingBanana(allBananaTargets, banana);
        }
    }

    private void checkIfAllBananasHaveATarget(BananaBomb bananaBomb, Iterable<Banana> bananas,
                                              Iterable<BananaTarget> targets) {
        for (Banana banana : bananas) {
            if(!BananaTarget.containsAssignableBanana(targets, banana)) {
                throw new IllegalArgumentException("BananaBomb "+bananaBomb+" doesn't contain a target for detonated banana "
                        +banana+". Add a radar with a target for this banana!");
            }
        }
    }

    private void checkIfAllTargetsHaveABanana(BananaBomb bananaBomb, Iterable<Banana> bananas,
                                              Iterable<BananaTarget> targets) {
        for (BananaTarget target : targets) {
            if (!Banana.containsAssignableTarget(bananas, target)) {
                throw new IllegalArgumentException("BananaBomb "+bananaBomb+" doesn't detonate a banana that can hit target "
                        +target+". Add a detonator for "+target.getBanana()+"!");
            }
        }
    }

}
