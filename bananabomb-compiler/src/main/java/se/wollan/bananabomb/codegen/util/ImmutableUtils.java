package se.wollan.bananabomb.codegen.util;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrEmpty;

/**
 * Created by aw on 2017-05-07.
 */

public class ImmutableUtils {

    public static <T> ImmutableList<T> concat(ImmutableList<T> first, T second) {
        return ImmutableList.<T>builder()
                .addAll(first)
                .add(second)
                .build();
    }

    public static <T> ImmutableList<T> concat(ImmutableList<T> first, ImmutableList<T> second) {
        return ImmutableList.<T>builder()
                .addAll(first)
                .addAll(second)
                .build();
    }

    public static <T> ImmutableSet<T> concat(ImmutableSet<T> first, T second) {
        return ImmutableSet.<T>builder()
                .addAll(first)
                .add(second)
                .build();
    }

    public static <T> ImmutableSet<T> concat(ImmutableSet<T> first, ImmutableSet<T> second) {
        return ImmutableSet.<T>builder()
                .addAll(first)
                .addAll(second)
                .build();
    }

    public static <T> T first(ImmutableCollection<T> source) {
        throwIfNullOrEmpty(source, "source");
        for (T t : source) {
            return t;
        }
        throw new IllegalStateException("should not happen");
    }
}
