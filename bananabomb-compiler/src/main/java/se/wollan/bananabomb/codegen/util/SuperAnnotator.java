package se.wollan.bananabomb.codegen.util;

import com.google.common.collect.ImmutableSet;

import java.lang.annotation.Annotation;

import javax.lang.model.type.TypeMirror;

import se.wollan.bananabomb.codegen.model.CanonicalName;

/**
 * Created by aw on 2017-06-10.
 */

public interface SuperAnnotator {

    ImmutableSet<ElementAndAnnotations> getElementsAnnotatedWithIncludingSubAnnotations(Class<? extends Annotation> annotation);
}

