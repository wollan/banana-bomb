package se.wollan.bananabomb.codegen.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import java.util.Collection;
import java.util.List;

import se.wollan.bananabomb.codegen.model.Banana;

/**
 * Created by aw on 2017-04-23.
 */

public class ArgumentChecker {

    public static String throwIfNullOrWhitespace(String argumentValue, String argumentName){

        if (argumentValue == null) {
            throw new IllegalArgumentException(argumentName + " cannot be null");
        }
        if (argumentValue.isEmpty()) {
            throw new IllegalArgumentException(argumentName + " cannot be empty");
        }

        //return if any char isn't whitespace
        for (int i = 0; i < argumentValue.length(); i++) {
            char c = argumentValue.charAt(i);
            if (c != ' ' && c != '\n' && c != '\t' && c != '\r') {
                return argumentValue;
            }
        }

        throw new IllegalArgumentException(argumentName + " cannot only be whitespaces: " + argumentValue);
    }

    public static String throwIfNotEndsWith(String argumentValue, String suffixToCheck, String argumentName){
        if (argumentValue == null || !argumentValue.endsWith(suffixToCheck)) {
            throw new IllegalArgumentException(argumentName + " must end with '" + suffixToCheck + "'");
        }

        return argumentValue;
    }

    public static <C extends Collection<?>> C throwIfNullOrEmpty(C argumentValue, String argumentName) {
        if (argumentValue == null) {
            throw new IllegalArgumentException(argumentName + " cannot be null");
        }
        if (argumentValue.isEmpty()) {
            throw new IllegalArgumentException(argumentName + " cannot be empty");
        }
        return argumentValue;
    }


    public static <T> T throwIfNull(T argumentValue, String argumentName) {
        if (argumentValue == null) {
            throw new IllegalArgumentException(argumentName + " cannot be null");
        }
        return argumentValue;
    }

    public static <E> Collection<E> throwIfNotSizeOne(Collection<E> argumentValue, String argumentName) {
        if (argumentValue == null) {
            throw new IllegalArgumentException(argumentName + " cannot be null");
        }
        if (argumentValue.size() != 1) {
            throw new IllegalArgumentException(argumentName + " must be of size one");
        }
        return argumentValue;
    }

    public static <E> List<E> throwIfNotSizeOne(List<E> argumentValue, String argumentName) {
        return (List<E>)throwIfNotSizeOne((Collection<E>)argumentValue, argumentName);
    }

    public static String substringAfterLastOrThrow(String argumentValue, String splitOn, String argumentName) {
        if (argumentValue == null) {
            throw new IllegalArgumentException(argumentName + " cannot be null");
        }
        int index = argumentValue.lastIndexOf(splitOn);
        if (index < 0) {
            throw new IllegalArgumentException("could not split " + argumentValue + " on " + splitOn);
        }
        return argumentValue.substring(index + 1);
    }

    public static String substringBeforeLastOrThrow(String argumentValue, String splitOn, String argumentName) {
        if (argumentValue == null) {
            throw new IllegalArgumentException(argumentName + " cannot be null");
        }
        int index = argumentValue.lastIndexOf(splitOn);
        if (index < 0) {
            throw new IllegalArgumentException("could not split " + argumentValue + " on " + splitOn);
        }
        return argumentValue.substring(0, index);
    }

    public static String throwIfNullOrContainsWhitespace(String argumentValue, String argumentName) {
        if (argumentValue == null) {
            throw new IllegalArgumentException(argumentName + " cannot be null");
        }
        if (argumentValue.isEmpty()) {
            throw new IllegalArgumentException(argumentName + " cannot be empty");
        }

        //throw on first whitespace
        for (int i = 0; i < argumentValue.length(); i++) {
            char c = argumentValue.charAt(i);
            if (c == ' ' || c == '\n' || c == '\t' || c == '\r') {
                throw new IllegalArgumentException(argumentName + " contains whitespaces: " + argumentValue);
            }
        }

        return argumentValue;
    }

    public static <T> ImmutableList<T> emptyIfNull(ImmutableList<T> list) {
        return list == null ? ImmutableList.<T>of() : list;
    }

    public static <T> ImmutableSet<T> emptyIfNull(ImmutableSet<T> list) {
        return list == null ? ImmutableSet.<T>of() : list;
    }

    public static <T> ImmutableList<T> appendOrThrow(ImmutableList<T> list, T item) {
        return ImmutableList.<T>builder()
                .addAll(throwIfNull(list, "list"))
                .add(throwIfNull(item, "item"))
                .build();
    }


    public static <E, I extends Iterable<E>> I throwIfNotContains(I argumentValue, E needle, String argumentName) {
        throwIfNull(argumentValue, argumentName);
        if(!Iterables.contains(argumentValue, needle)) {
            throw new IllegalArgumentException(argumentName + " must contain " + needle);
        }
        return argumentValue;
    }

    public static <T extends Iterable<?>> T throwIfNotEmpty(T argumentValue, String argumentName) {
        throwIfNull(argumentValue, argumentName);
        if (!Iterables.isEmpty(argumentValue)) {
            throw new IllegalArgumentException(argumentName + " must be be empy");
        }
        return argumentValue;
    }

    public static <E, I extends Iterable<E>> I throwIfContains(I argumentValue, E needle, String argumentName) {
        if (argumentValue == null) {
            return null; //null list does not contains anything!
        }
        if(Iterables.contains(argumentValue, needle)) {
            throw new IllegalArgumentException(argumentName + " must not contain " + needle);
        }
        return argumentValue;
    }
}
