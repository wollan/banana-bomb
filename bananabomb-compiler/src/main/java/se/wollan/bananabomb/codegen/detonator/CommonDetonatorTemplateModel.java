package se.wollan.bananabomb.codegen.detonator;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.BananaBombProcessor;
import se.wollan.bananabomb.codegen.processors.BananaProcessor;
import se.wollan.bananabomb.codegen.templating.Template;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.DateFormats.nowAsISO8601;

class CommonDetonatorTemplateModel {

    final CanonicalName processor = toCanonical(BananaProcessor.class);
    final String date = nowAsISO8601();
    final CanonicalName interfaceName;
    final BananaBomb bananaBomb;
    final ImmutableList<ImplementsModel> impl;
    boolean anyImpls() { return !impl.isEmpty(); }

    CommonDetonatorTemplateModel(BananaBomb bananaBomb, ImmutableSet<Banana> bananas) {

        ImmutableList.Builder<ImplementsModel> builder = ImmutableList.builder();
        ImmutableList<Banana> asList = bananas.asList();
        for (int i = 0; i < asList.size(); i++) {
            builder.add(new ImplementsModel(asList.get(i), i < bananas.size() - 1));
        }

        this.interfaceName = bananaBomb.getCanonical().replaceSimpleNameWith("Detonator");
        this.bananaBomb = bananaBomb;
        this.impl = builder.build();
    }

    static class ImplementsModel {
        final Banana banana;
        final boolean comma;

        ImplementsModel(Banana banana, boolean comma) {
            this.banana = banana;
            this.comma = comma;
        }
    }
}
