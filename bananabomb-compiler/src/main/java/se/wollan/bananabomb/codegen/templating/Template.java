package se.wollan.bananabomb.codegen.templating;

import se.wollan.bananabomb.codegen.model.CanonicalName;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrWhitespace;

/**
 * Created by aw on 2017-04-23.
 */

public class Template {

    private final String template;
    private final CanonicalName typeName;

    public Template(String template, CanonicalName typeName) {
        this.template = throwIfNullOrWhitespace(template, "template");
        this.typeName = throwIfNull(typeName, "typeName");
    }

    public String getTemplate() {
        return template;
    }

    public CanonicalName getTypeName() {
        return typeName;
    }
}
