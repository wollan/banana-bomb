package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.processors.PreviousResults;

/**
 * Created by aw on 2017-05-02.
 */

interface BananaTargetExtractor {

    ImmutableSet<BananaTarget> extract(BananaBomb bananaBomb, PreviousResults previousResults);
}
