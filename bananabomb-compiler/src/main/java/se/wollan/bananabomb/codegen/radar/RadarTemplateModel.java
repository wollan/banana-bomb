package se.wollan.bananabomb.codegen.radar;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.BananaBombProcessor;
import se.wollan.bananabomb.codegen.processors.BananaTargetProcessor;
import se.wollan.bananabomb.codegen.templating.Template;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.DateFormats.nowAsISO8601;

/**
 * Created by aw on 2017-04-23.
 */

class RadarTemplateModel {

    final CanonicalName processor = toCanonical(BananaTargetProcessor.class);
    final String date = nowAsISO8601();
    final BombingRange bombingRange;
    final CanonicalName interfaceName;
    final ImmutableSet<BananaTarget> targets;

    public RadarTemplateModel(BombingRange bombingRange, ImmutableSet<BananaTarget> targetsInRange) {
        this.bombingRange = bombingRange;
        this.interfaceName = bombingRange.getCanonical().withSuffix("Radar");
        this.targets = targetsInRange;
    }
}
