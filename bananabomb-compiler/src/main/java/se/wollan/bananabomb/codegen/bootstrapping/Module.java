package se.wollan.bananabomb.codegen.bootstrapping;

import com.google.common.collect.ImmutableList;

import javax.annotation.processing.ProcessingEnvironment;

import se.wollan.bananabomb.codegen.bomb.BananaBombModule;
import se.wollan.bananabomb.codegen.detonator.DetonatorModule;
import se.wollan.bananabomb.codegen.handler.HandlerModule;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.radar.RadarModule;

/**
 * Created by aw on 2017-04-24.
 */

public interface Module {

    ImmutableList<Module> MODULES = ImmutableList.of(
            new DetonatorModule(),
            new RadarModule(),
            new HandlerModule(),
            new BananaBombModule()
    );

    BombFactory createFactory(ProcessingEnvironment processingEnvironment);

}
