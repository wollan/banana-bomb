package se.wollan.bananabomb.codegen.util;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

import se.wollan.bananabomb.codegen.model.CanonicalName;

import static com.google.common.collect.ImmutableSet.toImmutableSet;

/**
 * Created by aw on 2017-06-10.
 */

public class SuperTyperImpl implements SuperTyper {

    private final Types types;

    public SuperTyperImpl(Types types) {
        this.types = types;
    }

    @Override
    public ImmutableSet<CanonicalName> getAllSupertypes(TypeMirror self) {

        //((TypeElement)((DeclaredType)types.directSupertypes(bananaBomb.getTypeElement().asType()).get(1)).asElement()).getInterfaces()

        CanonicalName selfCanon = new CanonicalName(self.toString());
        if (selfCanon.isTopLevel()) {
            return ImmutableSet.of();
        }

        List<CanonicalName> includingSelfAndObj = getTypeAndSupertypes(self);

        return includingSelfAndObj.stream()
                .filter(c -> !c.equals(selfCanon))
                .collect(toImmutableSet());
    }

    private List<CanonicalName> getTypeAndSupertypes(TypeMirror typeMirror) {
        TypeElement typeElement = (TypeElement) ((DeclaredType)typeMirror).asElement();
        List<CanonicalName> mirrors = new ArrayList<>();
        mirrors.add(new CanonicalName(typeMirror.toString()));
        List<? extends TypeMirror> supertypes = types.directSupertypes(typeMirror);
        List<? extends TypeMirror> superinterfaces = typeElement.getInterfaces();
        for (TypeMirror t : Iterables.concat(supertypes, superinterfaces)) {
            mirrors.addAll(getTypeAndSupertypes(t));
        }
        return mirrors;
    }
}
