package se.wollan.bananabomb.codegen.detonator;

import com.google.common.collect.ImmutableList;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.BananaBombProcessor;
import se.wollan.bananabomb.codegen.processors.BananaProcessor;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.DateFormats.nowAsISO8601;

class DetonatorTemplateModel {

    final CanonicalName processor = toCanonical(BananaProcessor.class);
    final String date = nowAsISO8601();
    final Banana banana;
    final CanonicalName interfaceName;

    DetonatorTemplateModel(Banana banana) {
        this.banana = banana;
        this.interfaceName = banana.getCanonical().withSuffix("Detonator");
    }
}
