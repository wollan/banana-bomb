package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableSet;

import java.util.List;

import javax.lang.model.type.TypeMirror;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.processors.GeneratedFileResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;
import se.wollan.bananabomb.codegen.util.SuperTyper;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-07.
 */

class PreviousResultBananaTargetExtractor implements BananaTargetExtractor {

    private final SuperTyper superTyper;

    PreviousResultBananaTargetExtractor(SuperTyper superTyper) {
        this.superTyper = superTyper;
    }

    @Override
    public ImmutableSet<BananaTarget> extract(BananaBomb bananaBomb, PreviousResults previousResults) {

        ImmutableSet<CanonicalName> supertypes = superTyper.getAllSupertypes(bananaBomb.getTypeElement().asType());

        ImmutableSet.Builder<BananaTarget> bananaBuilder = ImmutableSet.builder();

        for (CanonicalName supertype : supertypes) {

            GeneratedFileResult result = previousResults.getResultByGeneratedFileNameOrNull(supertype);
            if (result == null) {
                continue;
            }

            bananaBuilder.addAll(result.getUsedBananaTargets());
        }

        return bananaBuilder.build();
    }
}
