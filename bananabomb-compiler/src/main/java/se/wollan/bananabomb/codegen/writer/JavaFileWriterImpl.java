package se.wollan.bananabomb.codegen.writer;

import java.io.IOException;
import java.io.Writer;

import javax.annotation.processing.Filer;
import javax.annotation.processing.FilerException;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.JavaFileObject;

import static java.awt.SystemColor.text;

/**
 * Created by aw on 2017-04-22.
 */

public class JavaFileWriterImpl implements JavaFileWriter {

    private final Filer filer;

    public JavaFileWriterImpl(Filer filer) {
        this.filer = filer;
    }

    @Override
    public void write(JavaFile javaFile) throws IOException {
        try {
            JavaFileObject sourceFile =
                    filer.createSourceFile(javaFile.getTypeName().toString());

            try (Writer writer = sourceFile.openWriter()) {
                writer.write(javaFile.getFileContents());
            }
        } catch (FilerException e) {
            if (e.getMessage().startsWith("Attempt to recreate a file for type")) {
                //recreate file failures can safely be ignored due to same input data as the first round
            } else {
                throw e;
            }
        }
    }
}
