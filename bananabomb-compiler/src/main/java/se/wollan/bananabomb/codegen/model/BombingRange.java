package se.wollan.bananabomb.codegen.model;

import javax.lang.model.element.Element;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;

public class BombingRange {

    private final CanonicalName canonical;

    public BombingRange(CanonicalName canonical) {
        this.canonical = throwIfNull(canonical, "canonical");
    }

    public BombingRange(Element enclosingElement) {
        this(toCanonical(enclosingElement.toString()));
    }

    public BombingRange(Class klass) {
        this(toCanonical(klass.getCanonicalName()));
    }

    @Override
    public String toString() {
        return canonical.toString();
    }

    public CanonicalName getCanonical() {
        return canonical;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BombingRange that = (BombingRange) o;

        return canonical.equals(that.canonical);
    }

    @Override
    public int hashCode() {
        return canonical.hashCode();
    }
}
