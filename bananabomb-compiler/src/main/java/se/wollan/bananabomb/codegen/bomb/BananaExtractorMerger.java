package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableSet;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.processors.PreviousResults;

public class BananaExtractorMerger implements BananaExtractor {

    private final Iterable<BananaExtractor> bananaExtractors;

    public BananaExtractorMerger(Iterable<BananaExtractor> bananaExtractors) {
        this.bananaExtractors = bananaExtractors;
    }

    @Override
    public ImmutableSet<Banana> extract(BananaBomb bananaBomb, PreviousResults previousResults) {
        ImmutableSet.Builder<Banana> bananaBuilder = ImmutableSet.builder();
        for (BananaExtractor extractor : bananaExtractors) {
            bananaBuilder.addAll(extractor.extract(bananaBomb, previousResults));
        }
        return bananaBuilder.build();
    }
}
