package se.wollan.bananabomb.codegen.handler;

import javax.annotation.processing.ProcessingEnvironment;

import se.wollan.bananabomb.codegen.bootstrapping.Module;
import se.wollan.bananabomb.codegen.processors.BombFactory;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.templating.TemplaterImpl;
import se.wollan.bananabomb.codegen.util.ResourceLoaderImpl;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;
import se.wollan.bananabomb.codegen.writer.JavaFileWriterImpl;

/**
 * Created by aw on 2017-04-23.
 */

public class HandlerModule implements Module {

    @Override
    public BombFactory createFactory(ProcessingEnvironment env) {

        JavaFileWriter writer = new JavaFileWriterImpl(env.getFiler());
        Templater templater = new TemplaterImpl();
        ResourceLoaderImpl resLoader = new ResourceLoaderImpl();

        return new HandlerFactory(writer, templater, resLoader);
    }
}
