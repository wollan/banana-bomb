package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.appendOrThrow;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrWhitespace;

public class FactorResult {
    private final String nameOfFactory;
    ImmutableList<GeneratedFileResult> fileResults;

    public FactorResult(String nameOfFactory, ImmutableList<GeneratedFileResult> fileResults) {
        this.nameOfFactory = throwIfNullOrWhitespace(nameOfFactory, "nameOfFactory");
        this.fileResults = throwIfNull(fileResults, "fileResults");
    }

    public static FactorResult create(String nameOfFactory) {
        return new FactorResult(nameOfFactory, ImmutableList.<GeneratedFileResult>of());
    }

    public FactorResult append(GeneratedFileResult fileResult) {
        return new FactorResult(nameOfFactory, appendOrThrow(fileResults, fileResult));
    }

    public String getNameOfFactory() {
        return nameOfFactory;
    }

    public ImmutableList<GeneratedFileResult> getFileResults() {
        return fileResults;
    }
}
