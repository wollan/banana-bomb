package se.wollan.bananabomb.codegen.model;

import com.google.common.collect.ImmutableList;

import se.wollan.bananabomb.ExceptionalBanana;
import se.wollan.bananabomb.RottenBanana;

import static se.wollan.bananabomb.codegen.util.ArgumentChecker.substringAfterLastOrThrow;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.substringBeforeLastOrThrow;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNullOrContainsWhitespace;

public class CanonicalName {

    public static final CanonicalName JAVA_OBJECT = toCanonical(Object.class);
    public static final CanonicalName ROTTEN = toCanonical(RottenBanana.class);
    public static final CanonicalName EXCEPTIONAL = toCanonical(ExceptionalBanana.class);

    final String canonicalName;
    final String simpleName;
    final String simpleNameCamelCase;
    final String packageName;
    final ImmutableList<String> packageSplit;
    final String uniqueVariableName;
    final boolean isTopLevel;

    public CanonicalName(String canonicalName) {
        this.canonicalName = throwIfNullOrContainsWhitespace(canonicalName, "canonicalName");
        this.simpleName = throwIfNullOrContainsWhitespace(
                substringAfterLastOrThrow(canonicalName, ".", "simpleName"), "simpleName");
        this.simpleNameCamelCase = simpleName.substring(0, 1).toLowerCase() + simpleName.substring(1);
        this.packageName = throwIfNullOrContainsWhitespace(
                substringBeforeLastOrThrow(canonicalName, ".", "packageName"), "packageName");
        this.packageSplit = ImmutableList.copyOf(packageName.split("[.]"));
        this.uniqueVariableName = this.canonicalName.replace('.', '_');
        this.isTopLevel = isTopLevelClass(canonicalName);
    }

    private static boolean isTopLevelClass(String canonicalName) {
        return canonicalName.equals(Object.class.getCanonicalName())
                || canonicalName.equals(RottenBanana.class.getCanonicalName())
                || canonicalName.equals(ExceptionalBanana.class.getCanonicalName());
    }

    public static CanonicalName toCanonical(String value) {
        return new CanonicalName(value);
    }

    public static CanonicalName toCanonical(Class<?> aClass) {
        return new CanonicalName(aClass.getCanonicalName());
    }

    public static String fromCanonical(CanonicalName canonicalName) {
        return throwIfNull(canonicalName, "canonicalName").toString();
    }

    @Override
    public String toString() {
        return canonicalName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CanonicalName that = (CanonicalName) o;

        return canonicalName.equals(that.canonicalName);
    }

    @Override
    public int hashCode() {
        return canonicalName.hashCode();
    }

    public CanonicalName withSuffix(String typeNameSuffix) {
        return new CanonicalName(canonicalName + typeNameSuffix);
    }

    public String getSimpleName() {
        return simpleName;
    }

    public String getSimpleNameCamelCase() {
        return simpleNameCamelCase;
    }

    public String getPackageName() {
        return packageName;
    }

    public ImmutableList<String> getPackageSplit() {
        return packageSplit;
    }

    public String getUniqueVariableName() {
        return uniqueVariableName;
    }

    public boolean isTopLevel() {
        return isTopLevel;
    }

    public CanonicalName replaceSimpleNameWith(String newSimpleName) {
        return new CanonicalName(packageName + "." + newSimpleName);
    }

    public boolean isJavaObject() {
        return JAVA_OBJECT.equals(this);
    }
}
