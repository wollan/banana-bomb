package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import javax.lang.model.element.TypeElement;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.processors.PreviousResults;

/**
 * Created by aw on 2017-05-02.
 */

interface BananaExtractor {

    ImmutableSet<Banana> extract(BananaBomb bananaBomb, PreviousResults previousResults);
}

