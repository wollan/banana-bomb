package se.wollan.bananabomb.codegen.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.util.Comparator;
import java.util.Set;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;

import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ArgumentChecker.throwIfNull;

/**
 * Created by aw on 2017-05-01.
 */

public class BananaBomb {

    private final CanonicalName canonical;
    private final TypeElement typeElement;

    public BananaBomb(CanonicalName canonical, TypeElement typeElement) {
        this.canonical = throwIfNull(canonical, "canonical");
        this.typeElement = throwIfNull(typeElement, "typeElement");
    }

    public static ImmutableSet<BananaBomb> getAnnotatedBombs(RoundEnvironment env) {
        Set<? extends Element> annotatedElements =
                env.getElementsAnnotatedWith(se.wollan.bananabomb.BananaBomb.class);

        ImmutableSet.Builder<BananaBomb> bananaTypeBuilder = ImmutableSet.builder();
        for (TypeElement type : ElementFilter.typesIn(annotatedElements)) {
            String qualifiedName = type.getQualifiedName().toString();
            if (!type.getKind().isInterface()) {
                throw new IllegalArgumentException("Non interface types aren't allowed as banana bombs: " + qualifiedName);
            }
            bananaTypeBuilder.add(new BananaBomb(toCanonical(qualifiedName), type));
        }
        return bananaTypeBuilder.build();
    }

    public static BananaBomb mostTopLevelPackage(ImmutableSet<BananaBomb> bananaBombs) {
        if (bananaBombs.isEmpty()) {
            throw new IllegalArgumentException("bananaBombs cannot be empty");
        }

        ImmutableList<BananaBomb> sorted = ImmutableList.sortedCopyOf(new Comparator<BananaBomb>() {
            @Override
            public int compare(BananaBomb b1, BananaBomb b2) {
                int c = b1.getCanonical().getPackageSplit().size() - b2.getCanonical().getPackageSplit().size();
                if (c != 0) return c;
                return b2.getCanonical().toString().compareTo(b1.getCanonical().toString());
            }
        }, bananaBombs);

        return sorted.get(0);
    }

    public CanonicalName getCanonical() {
        return canonical;
    }

    public TypeElement getTypeElement() {
        return typeElement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BananaBomb that = (BananaBomb) o;

        return canonical.equals(that.canonical);
    }

    @Override
    public int hashCode() {
        return canonical.hashCode();
    }

    @Override
    public String toString() {
        return canonical.toString();
    }
}
