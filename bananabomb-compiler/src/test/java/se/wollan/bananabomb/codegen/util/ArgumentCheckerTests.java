package se.wollan.bananabomb.codegen.util;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

/**
 * Created by aw on 2017-05-07.
 */
@RunWith(JUnitParamsRunner.class)
public class ArgumentCheckerTests {

    @Test
    @Parameters(value = {
            "apa.bepa.cepa, ., cepa",
            "apa.bepa., ., "
    })
    public void substringAfterLastOrThrow_validInput_returnsExpected(String value, String splitOn, String expected) throws Exception {
        String actual = ArgumentChecker.substringAfterLastOrThrow(value, splitOn, "value");
        assertEquals(expected, actual);
    }

    @Test
    @Parameters(value = {
            "apa_bepa_cepa, .",
            "apabepa, ."
    })
    public void substringAfterLastOrThrow_invalidInput_throwsArgument(final String value, final String splitOn) throws Exception {
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                ArgumentChecker.substringAfterLastOrThrow(value, splitOn, "value");
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    @Parameters(value = {
            "apa.bepa.cepa, ., apa.bepa",
            "apa.bepa., ., apa.bepa"
    })
    public void substringBeforeLastOrThrow_validInput_returnsExpected(String value, String splitOn, String expected) throws Exception {
        String actual = ArgumentChecker.substringBeforeLastOrThrow(value, splitOn, "value");
        assertEquals(expected, actual);
    }

    @Test
    @Parameters(value = {
            "apa_bepa_cepa, .",
            "apabepa, ."
    })
    public void substringBeforeLastOrThrow_invalidInput_throwsArgument(final String value, final String splitOn) throws Exception {
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                ArgumentChecker.substringBeforeLastOrThrow(value, splitOn, "value");
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }


}