package se.wollan.bananabomb.codegen.model;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-07.
 */
@RunWith(JUnitParamsRunner.class)
public class CanonicalNameTests {


    @Test
    @Parameters(value = {"", "Apa", "Apa_Bepa"})
    public void ctor_invalidValue_throwsArgument(final String invalidValue) throws Exception {

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new CanonicalName(invalidValue);
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    @Parameters(value = {
            "apa.bepa.Cepa, Cepa"
    })
    public void getSimpleName_validCanonicalName_returnsLastPart(final String validValue, String expected) throws Exception {
        CanonicalName sut = new CanonicalName(validValue);

        assertEquals(expected, sut.getSimpleName());
    }

    @Test
    @Parameters(value = {
            "apa.bepa.Cepa, cepa"
    })
    public void getSimpleNameCamelCase_validCanonicalName_returnsLastPartCamel(final String validValue, String expected) throws Exception {
        CanonicalName sut = new CanonicalName(validValue);

        assertEquals(expected, sut.getSimpleNameCamelCase());
    }

    @Test
    @Parameters(value = {
            "apa.bepa.Cepa, apa.bepa"
    })
    public void getPackageName_validCanonicalName_returnsAllButLastPart(final String validValue, String expected) throws Exception {
        CanonicalName sut = new CanonicalName(validValue);

        assertEquals(expected, sut.getPackageName());
    }

    @Test
    public void getUniqueVariableName_validValue_returnsExpected() throws Exception {
        CanonicalName sut = new CanonicalName("apa.bepa.Cepa");

        assertEquals("apa_bepa_Cepa", sut.getUniqueVariableName());
    }

    @Test
    @Parameters(value = {
            "se.wollan.bananabomb.RottenBanana, true",
            "se.wollan.bananabomb.ExceptionalBanana, true",
            "java.lang.Object, true",
            "apa.bepa.Cepa, false",
    })
    public void isTopLevel_givenValidName_shouldReturnExpected(String canonical, boolean expected) throws Exception {
        CanonicalName sut = new CanonicalName(canonical);

        boolean actual = sut.isTopLevel();

        assertEquals(expected, actual);

    }
}