package se.wollan.bananabomb.codegen.assertutils;

import org.hamcrest.CustomTypeSafeMatcher;
import org.mockito.Matchers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static se.wollan.bananabomb.codegen.assertutils.Assert.toJson;

/**
 * Created by aw on 2017-05-15.
 */

public class JsonMatcher<T> extends CustomTypeSafeMatcher<T> {
    private final String expectedJson;

    private JsonMatcher(String expectedJson) {
        super(expectedJson);
        this.expectedJson = expectedJson;
    }

    @Override
    protected boolean matchesSafely(T actual) {
        String actualJson = toJson(actual);
        return expectedJson.equals(actualJson);
    }

    public static <T> T eqJsonOf(T expected) {
        String expectedJson = toJson(expected);
        return Matchers.argThat(new JsonMatcher<T>(expectedJson));
    }
}
