package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableSet;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Test;

import se.wollan.bananabomb.codegen.fakes.EmptyTypeElement;
import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static se.wollan.bananabomb.codegen.assertutils.Assert.assertJsonEquals;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-15.
 */
public class BananaBombTemplateModelTests {

    @Test
    public void construct_withOneRottenBanana_shouldSetRottenDetonator() throws Exception {
        BananaBomb bananaBomb = new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement());
        BananaTarget bananaTarget = new BananaTarget(new BombingRange(toCanonical("some.Range")),
                "on", Banana.ROTTEN);

        BananaBombTemplateModel sut = new BananaBombTemplateModel(bananaBomb,
                ImmutableSet.<Banana>of(),
                ImmutableSet.of(bananaTarget));

        assertJsonEquals(new BananaBombTemplateModel.DetonateMethodModel(Banana.ROTTEN,
                        ImmutableSet.of(bananaTarget)),
                sut.detonateRottenBanana);
    }

    @Test
    public void construct_withNoneRottenBanana_shouldntSetRottenDetonator() throws Exception {
        BananaBomb bananaBomb = new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement());
        Banana banana = new Banana(toCanonical("some.Banana"));
        BananaTarget bananaTarget = new BananaTarget(new BombingRange(toCanonical("some.Range")), "on", banana);

        BananaBombTemplateModel sut = new BananaBombTemplateModel(bananaBomb,
                ImmutableSet.of(banana),
                ImmutableSet.of(bananaTarget));

        assertEquals(null, sut.detonateRottenBanana);
    }

    @Test
    public void construct_givenBombWithOnlyOneDetonator_throwsArgument() throws Exception {
        final BananaBomb bb = new BananaBomb(toCanonical("what.Ever"), new EmptyTypeElement());
        final Banana banana = new Banana(toCanonical("some.Banana")); //from detonate(..)

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new BananaBombTemplateModel(bb,
                        ImmutableSet.of(banana),
                        ImmutableSet.<BananaTarget>of());
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
        assertThat(actual.getMessage(), containsString("what.Ever"));
        assertThat(actual.getMessage(), containsString("some.Banana"));
    }

    @Test
    public void construct_givenDetonatorWithBananaAssignableToOneTarget_doesNotThrow() throws Exception {
        final BananaBomb bb = new BananaBomb(toCanonical("what.Ever"), new EmptyTypeElement());
        final Banana banana = new Banana(toCanonical("some.Banana")); //in target
        final Banana extendedBanana = banana.extend("some.Extended"); //will detonate
        final BananaTarget target = new BananaTarget(
                new BombingRange(toCanonical("some.Range")), "on", banana);

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new BananaBombTemplateModel(bb,
                        ImmutableSet.of(extendedBanana),
                        ImmutableSet.of(target));
            }
        });

        assertNull(actual);
    }

    @Test
    public void construct_givenBombWithOnlyOneRadar_throwsArgument() throws Exception {
        final BananaBomb bb = new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement());
        final BananaTarget bananaTarget = new BananaTarget(new BombingRange(toCanonical("some.BR")), "on",
                new Banana(toCanonical("some.Banana")));  //from radar

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new BananaBombTemplateModel(bb,
                        ImmutableSet.<Banana>of(),
                        ImmutableSet.of(bananaTarget));
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
        assertThat(actual.getMessage(), containsString("some.Bomb"));
        assertThat(actual.getMessage(), containsString("some.BR.on(some.Banana)"));
    }

    @Test
    public void construct_givenBombContainingOneRottenTarget_addsRottenTargetBananaToTemplateModel() throws Exception {
        BananaBomb bb = new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement());
        Banana banana = Banana.ROTTEN;
        BananaTarget bananaTarget = new BananaTarget(new BombingRange(toCanonical("some.Range")), "on", banana);

        BananaBombTemplateModel actual = new BananaBombTemplateModel(bb,
                ImmutableSet.<Banana>of(),
                ImmutableSet.of(bananaTarget));

        assertJsonEquals(ImmutableSet.of(Banana.ROTTEN), actual.bananas);
    }

    @Test
    public void construct_givenRottenDetonator_throwsArgument() throws Exception {
        final BananaBomb bb = new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement());
        final Banana banana = Banana.ROTTEN; //means rotten detonator
        final BananaTarget bananaTarget = new BananaTarget(new BombingRange(toCanonical("some.Range")), "on", banana);

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new BananaBombTemplateModel(bb,
                        ImmutableSet.of(banana),
                        ImmutableSet.of(bananaTarget));
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
        assertThat(actual.getMessage(), containsString("some.Bomb"));
        assertThat(actual.getMessage(), containsString("rotten"));
    }

    @Test
    public void construct_givenExceptionalDetonator_throwsArgument() throws Exception {
        final BananaBomb bb = new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement());
        final Banana banana = Banana.EXCEPTIONAL;
        final BananaTarget bananaTarget = new BananaTarget(new BombingRange(toCanonical("some.Range")), "on", banana);

        Throwable actual = catchThrowable(() -> new BananaBombTemplateModel(bb,
                ImmutableSet.of(banana),
                ImmutableSet.of(bananaTarget)));

        assertThat(actual, instanceOf(IllegalArgumentException.class));
        assertThat(actual.getMessage(), containsString("some.Bomb"));
        assertThat(actual.getMessage(), containsString("exceptional"));
    }


}