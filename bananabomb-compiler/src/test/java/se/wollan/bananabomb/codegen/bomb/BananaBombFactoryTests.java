package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import se.wollan.bananabomb.codegen.fakes.EmptyTypeElement;
import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.processors.FactorResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;
import se.wollan.bananabomb.codegen.templating.Template;
import se.wollan.bananabomb.codegen.templating.Templater;
import se.wollan.bananabomb.codegen.util.ResourceLoader;
import se.wollan.bananabomb.codegen.writer.JavaFileWriter;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static se.wollan.bananabomb.codegen.assertutils.JsonMatcher.eqJsonOf;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-14.
 */
public class BananaBombFactoryTests {

    public @Rule MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock Templater templater;
    @Mock BananaExtractor bananaExtractor;
    @Mock BananaTargetExtractor bananaTargetExtractor;
    @Mock JavaFileWriter fileWriter;
    @Mock ResourceLoader resourceLoader;
    @Mock PreviousResultBananaExtractor previousResultBananaExtractor;
    @Mock PreviousResultBananaTargetExtractor previousResultBananaTargetExtractor;
    @InjectMocks BananaBombFactory sut;


//    @Test
//    public void factor_givenBombContainingOneRottenTarget_addsRottenTargetBananaToTemplateModel() throws Exception {
//        PreviousResults prs = new PreviousResults(ImmutableList.<FactorResult>of());
//        BananaBomb bb = new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement());
//        Banana banana = Banana.ROTTEN;
//        BananaTarget bananaTarget = new BananaTarget(new BombingRange(toCanonical("some.Range")), "on", banana);
//        when(bananaExtractor.extract(bb)).thenReturn(ImmutableSet.<Banana>of());
//        when(bananaTargetExtractor.extract(bb)).thenReturn(ImmutableSet.of(bananaTarget));
//        when(previousResultBananaExtractor.extract(bb, prs)).thenReturn(ImmutableSet.<Banana>of());
//        when(previousResultBananaTargetExtractor.extract(bb, prs)).thenReturn(ImmutableSet.<BananaTarget>of());
//        when(resourceLoader.loadResourceDataByName(any(String.class))).thenReturn("<template>");
//
//        sut.factor(ImmutableList.<Banana>of(), ImmutableList.<BananaTarget>of(), ImmutableList.of(bb), prs);
//
//        BananaBombTemplateModel expected = new BananaBombTemplateModel(bb, ImmutableSet.of(banana),
//                ImmutableSet.of(bananaTarget));
//        verify(templater).render(any(Template.class), eqJsonOf(expected));
//    }


}