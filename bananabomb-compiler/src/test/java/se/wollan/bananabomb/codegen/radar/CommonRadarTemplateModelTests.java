package se.wollan.bananabomb.codegen.radar;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.lang.model.element.TypeElement;

import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.CanonicalName;

import static org.junit.Assert.*;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-06-09.
 */
public class CommonRadarTemplateModelTests {

    public @Rule MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock TypeElement typeElement;

    @Test
    public void anyImpls_givenNoRanges_returnsFalse() throws Exception {
        CommonRadarTemplateModel sut = new CommonRadarTemplateModel(
                new BananaBomb(toCanonical("apa.Bepa"), typeElement),
                ImmutableSet.of()
        );

        boolean actual = sut.anyImpls();

        assertFalse(actual);
    }

    @Test
    public void anyImpls_givenOneRange_returnsTrue() throws Exception {
        CommonRadarTemplateModel sut = new CommonRadarTemplateModel(
                new BananaBomb(toCanonical("apa.Bepa"), typeElement),
                ImmutableSet.of(new BombingRange(toCanonical("apa.Bepa")))
        );

        boolean actual = sut.anyImpls();

        assertTrue(actual);
    }

}