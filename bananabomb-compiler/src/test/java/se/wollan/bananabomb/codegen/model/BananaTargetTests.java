package se.wollan.bananabomb.codegen.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static se.wollan.bananabomb.codegen.assertutils.Assert.assertJsonEquals;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-04-23.
 */
public class BananaTargetTests {
    @Rule public MockitoRule rule = MockitoJUnit.rule();


    @Rule public ExpectedException expectedException = ExpectedException.none();
    @Test
    public void uniqueBombingRanges() throws Exception {
        ImmutableList<BananaTarget> targets = ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on2", new Banana(toCanonical("se.wollan.B2"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Bepa")), "on1", new Banana(toCanonical("se.wollan.B2")))
        );

        ImmutableSet<BombingRange> actual = BananaTarget.uniqueBombingRanges(targets);

        ImmutableSet<BombingRange> expacted = ImmutableSet.of(
                new BombingRange(toCanonical("se.wollan.Apa")),
                new BombingRange(toCanonical("se.wollan.Bepa"))
        );
        assertJsonEquals(expacted, actual);
    }

    @Test
    public void getSingleBomingRangeOrThrow_validInput_returnsClass() throws Exception {
        ImmutableList<BananaTarget> targets = ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on1", new Banana(toCanonical("se.wollan.B1"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on2", new Banana(toCanonical("se.wollan.B2")))
        );

        BombingRange actual = BananaTarget.getSingleBomingRangeOrThrow(targets);

        assertJsonEquals(new BombingRange(toCanonical("se.wollan.Apa")), actual);
    }

    @Test
    public void getSingleBomingRangeOrThrow_invalidInput_throws() throws Exception {
        ImmutableList<BananaTarget> targets = ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on1", new Banana(toCanonical("se.wollan.B1"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on2", new Banana(toCanonical("se.wollan.B2"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Bepa")), "on1", new Banana(toCanonical("se.wollan.B2")))
        );

        expectedException.expect(IllegalArgumentException.class);
        BananaTarget.getSingleBomingRangeOrThrow(targets);
    }

    @Test
    public void targetsWithinRange() throws Exception {
        ImmutableList<BananaTarget> targets = ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on1", new Banana(toCanonical("se.wollan.B1"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on2",  new Banana(toCanonical("se.wollan.B2"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Bepa")), "on1", new Banana(toCanonical("se.wollan.B2")))
        );

        ImmutableSet<BananaTarget> actual = BananaTarget.targetsWithinRange(
                targets, new BombingRange(toCanonical("se.wollan.Apa")));

        assertJsonEquals(ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on1", new Banana(toCanonical("se.wollan.B1"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")), "on2", new Banana(toCanonical("se.wollan.B2")))
        ), actual);
    }

    @Test
    public void getBombingRangesMatchingBanana() throws Exception {
        ImmutableList<BananaTarget> targets = ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Cepa")), "on", new Banana(toCanonical("se.wollan.B1"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")),  "on", new Banana(toCanonical("se.wollan.B1"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Apa")),  "on", new Banana(toCanonical("se.wollan.B2"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Bepa")), "on", new Banana(toCanonical("se.wollan.B2"))),
                new BananaTarget(new BombingRange(toCanonical("se.wollan.Depa")), "on", new Banana("se.wollan.B2").extend("se.wollan.ExtB2"))
        );

        ImmutableSet<BombingRange> actual = BananaTarget.getBombingRangesMatchingBanana(
            targets, new Banana("se.wollan.B2").extend("se.wollan.ExtB2"));

        assertJsonEquals(ImmutableList.<BombingRange>of(
                new BombingRange(toCanonical("se.wollan.Apa")),
                new BombingRange(toCanonical("se.wollan.Bepa")),
                new BombingRange(toCanonical("se.wollan.Depa"))
        ), actual);
    }

    @Test
    public void hasRottenTarget_targetWithOneRotten_returnsTrue() throws Exception {
        ImmutableList<BananaTarget> targets = ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("some.Range0")), "on",
                    new Banana(toCanonical("some.Banana0"))),
                new BananaTarget(new BombingRange(toCanonical("some.Range1")), "on",
                    Banana.ROTTEN));

        boolean actual = BananaTarget.hasRottenTarget(targets);

        assertTrue(actual);
    }

    @Test
    public void hasRottenTarget_targetWithZeroRotten_returnsFalse() throws Exception {
        ImmutableList<BananaTarget> targets = ImmutableList.of(
                new BananaTarget(new BombingRange(toCanonical("some.Range0")), "on",
                    new Banana(toCanonical("some.Banana0"))),
                new BananaTarget(new BombingRange(toCanonical("some.Range1")), "on",
                    new Banana(toCanonical("some.Banana1"))));

        boolean actual = BananaTarget.hasRottenTarget(targets);

        assertFalse(actual);
    }

    @Test
    public void enrichBananas_giveOneUnrichedTargetAndOneRichBanana_returnEnrichedTarget() throws Exception {
        ImmutableSet<BananaTarget> targets = ImmutableSet.of(new BananaTarget(
                new BombingRange(toCanonical("some.Range")), "on", new Banana("some.Banana")
        ));
        Banana enrichedbanana = new Banana(toCanonical("some.Banana"),
                ImmutableSet.of(Banana.JAVA_OBJECT), ImmutableSet.of(Cluster.TOP_LEVEL));

        ImmutableSet<BananaTarget> actual = BananaTarget.enrichBananas(targets, ImmutableSet.of(enrichedbanana));

        ImmutableSet<BananaTarget> expected = ImmutableSet.of(new BananaTarget(
                new BombingRange(toCanonical("some.Range")), "on", enrichedbanana
        ));
        assertJsonEquals(expected, actual);
    }
}