package se.wollan.bananabomb.codegen.detonator;

import com.google.common.collect.ImmutableSet;

import org.junit.Test;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.CanonicalName;
import se.wollan.bananabomb.codegen.model.Cluster;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;
import static se.wollan.bananabomb.codegen.model.Banana.JAVA_OBJECT;
import static se.wollan.bananabomb.codegen.model.Banana.toBanana;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.model.Cluster.toCluster;

/**
 * Created by aw on 2017-06-11.
 */
public class ClusterDetonatorTemplateModelTests {

    @Test
    public void construct_withUnknownClusterBanana_throwsArgument() throws Exception {

        Throwable actual = catchThrowable(() -> new ClusterDetonatorTemplateModel(
                toCluster("some.Cluster"),
                ImmutableSet.of(new Banana("some.Banana"))
        ));

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void construct_withUnmatchingClusterBanana_throwsArgument() throws Exception {

        Throwable actual = catchThrowable(() -> new ClusterDetonatorTemplateModel(
                toCluster("some.Cluster"),
                ImmutableSet.of(new Banana(toCanonical("some.Banana"),
                        ImmutableSet.of(Banana.JAVA_OBJECT),
                        ImmutableSet.of(Cluster.TOP_LEVEL, toCluster("some.other.Cluster"))))
        ));

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }
}