package se.wollan.bananabomb.codegen.assertutils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import static org.junit.Assert.assertEquals;

/**
 * Created by aw on 2017-04-23.
 */

public class Assert {

    private static final Gson GSON = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    public static <T> void assertJsonEquals(T expected, T actual) {

        String expectedJson = toJson(expected);
        String actualJson = toJson(actual);

        assertEquals(expectedJson, actualJson);
    }

    public static String toJson(Object src) {
        return GSON.toJson(src);
    }

    public <T> T fromJson(String json, Class<T> classOfT) throws JsonSyntaxException {
        return GSON.fromJson(json, classOfT);
    }

}
