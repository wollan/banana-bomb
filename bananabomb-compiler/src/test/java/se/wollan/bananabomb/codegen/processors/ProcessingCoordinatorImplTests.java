package se.wollan.bananabomb.codegen.processors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collections;

import javax.annotation.processing.Messager;
import javax.tools.Diagnostic;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by aw on 2017-06-11.
 */
public class ProcessingCoordinatorImplTests {

    public @Rule MockitoRule rule = MockitoJUnit.rule();
    @Mock Messager messager;
    @Mock BombFactory bombFactory;

    private ProcessingCoordinatorImpl createSut() {
        return new ProcessingCoordinatorImpl(Collections.singleton(bombFactory), messager);
    }

    @Test
    public void coordinateBananas_throwWhileFactoring_printsMessagerError() throws Exception {
        when(bombFactory.factor(any(), any(), any(), any()))
                .thenThrow(new IllegalArgumentException("HELLO"));
        ProcessingCoordinatorImpl sut = createSut();

        sut.coordinateBananas(ImmutableSet.of());

        verify(messager).printMessage(eq(Diagnostic.Kind.ERROR), contains("HELLO"));
    }
}