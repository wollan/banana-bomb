package se.wollan.bananabomb.codegen.model;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableSet;

import org.assertj.core.api.ThrowableAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import se.wollan.bananabomb.ExceptionalBanana;
import se.wollan.bananabomb.RottenBanana;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;
import static se.wollan.bananabomb.codegen.assertutils.Assert.assertJsonEquals;
import static se.wollan.bananabomb.codegen.model.CanonicalName.JAVA_OBJECT;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.model.Cluster.toCluster;

/**
 * Created by aw on 2017-04-23.
 */
@RunWith(JUnitParamsRunner.class)
public class BananaTests {

    @Test
    public void extend_object_asAssignableToContainsObject() throws Exception {
        Banana actual = Banana.JAVA_OBJECT.extend("some.Banana");

        assertJsonEquals(ImmutableSet.of(Banana.JAVA_OBJECT), actual.getAssignableTo());
    }

    @Test
    public void extend_someBanana_asAssignableToContainsObjectAndSuper() throws Exception {
        Banana actual = Banana.JAVA_OBJECT.extend("some.SuperBanana").extend("some.SubBanana");

        assertJsonEquals(ImmutableSet.of(Banana.JAVA_OBJECT, new Banana("some.SuperBanana")),
                actual.getAssignableTo());
    }

    @Test
    public void isRotten_givenRotten_returnsTrue() throws Exception {
        assertTrue(Banana.ROTTEN.isRotten());
    }

    @Test
    public void ROTTEN_shouldEqualBananaFromRottenBananaClass() throws Exception {
        assertEquals(new Banana(RottenBanana.class), Banana.ROTTEN);
    }

    @Test
    public void isExceptional_givenExceptional_returnsTrue() throws Exception {
        assertTrue(Banana.EXCEPTIONAL.isExceptional());
    }

    @Test
    public void EXCEPTIONAL_shouldEqualBananaFromExceptionalBananaClass() throws Exception {
        assertEquals(new Banana(ExceptionalBanana.class), Banana.EXCEPTIONAL);
    }

    @Test
    public void JAVA_OBJECT_shouldEqualBananaFromJavaObject() throws Exception {
        assertEquals(new Banana(Object.class), Banana.JAVA_OBJECT);
    }

    @Test
    public void construct_withSelfAsAssignable_throwsArgument() throws Exception {
        final Banana b0 = new Banana("some.Banana");

        Throwable actual = catchThrowable(() -> new Banana("some.Banana", ImmutableSet.of(Banana.JAVA_OBJECT, b0)));

        assertThat(actual, instanceOf(IllegalArgumentException.class));
        assertThat(actual.getMessage(), containsString("assignableTo"));
        assertThat(actual.getMessage(), containsString("some.Banana"));
    }

    public static class SomeBanana { }

    @Test
    public void construct_emptyAssignableTo_throwsArgument() throws Exception {
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new Banana(toCanonical(String.class), ImmutableSet.<Banana>of(), ImmutableSet.of());
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    @Parameters(method = "isAssignableToTestData")
    public void isAssignableTo_givenBanana_returnsExpected(Banana sut, Banana arg, boolean expected) throws Exception {
        boolean actual = sut.isAssignableTo(arg);
        assertEquals(expected, actual);
    }

    public static List<Object[]> isAssignableToTestData() {
        return Arrays.asList(new Object[][]{
                {Banana.ROTTEN, new Banana(Object.class), false},
                {new Banana(RottenBanana.class), Banana.JAVA_OBJECT, false},
                {new Banana("some.Banana"), new Banana(Object.class), true},
                {new Banana(Object.class), new Banana("some.Banana"), false},
                {new Banana("some.Banana"), Banana.ROTTEN, false},
                {new Banana("some.Banana").extend("some.SubBanana"), new Banana("some.Banana"), true},
                {new Banana("some.Banana"), new Banana("some.Banana"), true},
                {new Banana("some.Banana").extend("some.SubBanana"), new Banana("some.SubBanana"), true},
                {new Banana("some.SubBanana"), new Banana("some.Banana"), false},
        });
    }

    @Test
    public void getBelongsTo_withoutSettingCluster_shouldDefaultToUnknown() throws Exception {
        Banana sut = new Banana("apa.Bepa");

        assertEquals(ImmutableSet.of(), sut.getBelongsTo());
    }

    @Test
    public void unknownClusters_withoutSettingCluster_returnsTrue() throws Exception {
        Banana sut = new Banana("apa.Bepa");

        assertTrue(sut.unknownClusters());
    }

    @Test
    public void construct_oneClusterThatIsntTopLevel_throwsArg() throws Exception {
        Throwable actual = catchThrowable(() -> new Banana(toCanonical("some.Banana"),
                ImmutableSet.of(Banana.JAVA_OBJECT),
                ImmutableSet.of(toCluster("some.Cluster"))));

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }
}