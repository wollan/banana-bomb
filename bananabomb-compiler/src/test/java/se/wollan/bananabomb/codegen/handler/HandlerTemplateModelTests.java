package se.wollan.bananabomb.codegen.handler;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Test;

import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaTarget;
import se.wollan.bananabomb.codegen.model.BombingRange;
import se.wollan.bananabomb.codegen.model.CanonicalName;

import static org.junit.Assert.*;
import static se.wollan.bananabomb.codegen.assertutils.Assert.assertJsonEquals;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;
import static se.wollan.bananabomb.codegen.util.ImmutableUtils.first;

/**
 * Created by aw on 2017-05-20.
 */
public class HandlerTemplateModelTests {

    @Test
    public void construct_twoTargetWhichTakesSameBanana_singleSendMethodModelShouldContainBoth() throws Exception {
        BombingRange bombingRange = new BombingRange(toCanonical("some.Bomb"));
        Banana banana = new Banana(toCanonical("some.Banana"));
        BananaTarget on0 = new BananaTarget(bombingRange, "on0", banana);
        BananaTarget on1 = new BananaTarget(bombingRange, "on1", banana);

        HandlerTemplateModel sut = new HandlerTemplateModel(ImmutableSet.of(on0, on1));

        assertEquals(1, sut.sendMethods.size());
        HandlerTemplateModel.SendMethodModel actual = first(sut.sendMethods);
        assertJsonEquals(ImmutableSet.of(new HandlerTemplateModel.TargetModel(on0, 0),
                new HandlerTemplateModel.TargetModel(on1, 1)), actual.targets);
    }

    @Test
    public void construct_wildcardTarget_sendMethodModelShouldContainAllAssignableTargets() throws Exception {
        BombingRange bombingRange = new BombingRange(toCanonical("some.Bomb"));
        Banana banana = new Banana(toCanonical("some.Banana"));
        Banana extendedbanana = banana.extend("some.ExtendedBanana");
        BananaTarget on = new BananaTarget(bombingRange, "on", banana);
        BananaTarget onExt = new BananaTarget(bombingRange, "on", extendedbanana);

        HandlerTemplateModel sut = new HandlerTemplateModel(ImmutableSet.of(on, onExt));

        assertJsonEquals(new HandlerTemplateModel.SendMethodModel(banana, ImmutableList.of(
                new HandlerTemplateModel.TargetModel(on, 0))), sut.sendMethods.get(0));
        assertJsonEquals(new HandlerTemplateModel.SendMethodModel(extendedbanana, ImmutableList.of(
                new HandlerTemplateModel.TargetModel(on, 0),
                new HandlerTemplateModel.TargetModel(onExt, 1))), sut.sendMethods.get(1));
    }

    @Test
    public void construct_oneExceptionalTarget_targetModelIdShouldBeMinusOne() throws Exception {
        BombingRange bombingRange = new BombingRange(toCanonical("some.Bomb"));
        Banana banana = Banana.EXCEPTIONAL;
        BananaTarget on0 = new BananaTarget(bombingRange, "on0", banana);

        HandlerTemplateModel sut = new HandlerTemplateModel(ImmutableSet.of(on0));

        assertJsonEquals(ImmutableSet.of(new HandlerTemplateModel.TargetModel(on0, -1)), sut.targets);
    }

    @Test
    public void construct_twoExceptionalAndThreeOrdinary_twoWillAndThreeWontCatchTargets() throws Exception {
        BombingRange bombingRange = new BombingRange(toCanonical("some.Bomb"));
        BananaTarget onM2 = new BananaTarget(bombingRange, "onM2", Banana.EXCEPTIONAL);
        BananaTarget onM1 = new BananaTarget(bombingRange, "onM1", Banana.EXCEPTIONAL);
        BananaTarget onP0 = new BananaTarget(bombingRange, "onP0", new Banana("some.Banana0"));
        BananaTarget onP1 = new BananaTarget(bombingRange, "onP1", new Banana("some.Banana1"));
        BananaTarget onP2 = new BananaTarget(bombingRange, "onP2", new Banana("some.Banana2"));

        HandlerTemplateModel sut = new HandlerTemplateModel(ImmutableSet.of(onM2, onM1, onP0, onP1, onP2));

        assertJsonEquals(ImmutableSet.of(
                new HandlerTemplateModel.TargetModel(onM2, -1),
                new HandlerTemplateModel.TargetModel(onM1, -2)), sut.wontCatchTargets);
        assertJsonEquals(ImmutableSet.of(
                new HandlerTemplateModel.TargetModel(onP0, 0),
                new HandlerTemplateModel.TargetModel(onP1, 1),
                new HandlerTemplateModel.TargetModel(onP2, 2)), sut.willCatchTargets);
    }
}