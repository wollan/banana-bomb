package se.wollan.bananabomb.codegen.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.junit.Assert.*;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-04-23.
 */
@RunWith(JUnitParamsRunner.class)
public class
BombingRangeTests {

    @Rule public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void equal_twoWithSameQualifiedName_returnsTrue() throws Exception {

        BombingRange targetClass1 = new BombingRange(toCanonical("com.example.Apa"));
        BombingRange targetClass2 = new BombingRange(toCanonical("com.example.Apa"));

        assertEquals(targetClass1, targetClass2);
    }

    @Test
    @Parameters(value = {
            "   . sdf.    sdf",
            "",
    })
    public void construct_invalidValues_throwsArgument(String qualified) throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        new BombingRange(toCanonical(qualified));
    }


}