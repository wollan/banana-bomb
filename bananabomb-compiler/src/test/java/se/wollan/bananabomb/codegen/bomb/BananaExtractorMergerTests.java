package se.wollan.bananabomb.codegen.bomb;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import se.wollan.bananabomb.codegen.fakes.EmptyTypeElement;
import se.wollan.bananabomb.codegen.model.Banana;
import se.wollan.bananabomb.codegen.model.BananaBomb;
import se.wollan.bananabomb.codegen.processors.FactorResult;
import se.wollan.bananabomb.codegen.processors.PreviousResults;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static se.wollan.bananabomb.codegen.assertutils.Assert.assertJsonEquals;
import static se.wollan.bananabomb.codegen.model.CanonicalName.toCanonical;

/**
 * Created by aw on 2017-05-18.
 */
public class BananaExtractorMergerTests {

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock BananaExtractor extractor0;
    @Mock BananaExtractor extractor1;

    @Test
    public void extract_withTwoExtractors_mergesBothResults() throws Exception {
        when(extractor0.extract(any(BananaBomb.class), any(PreviousResults.class)))
                .thenReturn(ImmutableSet.<Banana>of(new Banana(toCanonical("some.Banana0"))));
        when(extractor1.extract(any(BananaBomb.class), any(PreviousResults.class)))
                .thenReturn(ImmutableSet.<Banana>of(new Banana(toCanonical("some.Banana1"))));
        BananaExtractorMerger sut = new BananaExtractorMerger(ImmutableList.of(extractor0, extractor1));

        ImmutableSet<Banana> actual = sut.extract(new BananaBomb(toCanonical("some.Bomb"), new EmptyTypeElement()),
                new PreviousResults(ImmutableList.<FactorResult>of()));

        assertJsonEquals(
                ImmutableSet.of(new Banana(toCanonical("some.Banana0")), new Banana(toCanonical("some.Banana1"))),
                actual);
    }

}