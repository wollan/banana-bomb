#!/bin/bash
set -ex

mkdir -p $ANDROID_HOME/ndk-bundle/platforms
mkdir -p $ANDROID_HOME/ndk-bundle/toolchains
yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses
$ANDROID_HOME/tools/bin/sdkmanager ndk-bundle
ln -s $ANDROID_HOME/ndk-bundle/toolchains/aarch64-linux-android-4.9 $ANDROID_HOME/ndk-bundle/toolchains/mips64el-linux-android
ln -s $ANDROID_HOME/ndk-bundle/toolchains/arm-linux-androideabi-4.9 $ANDROID_HOME/ndk-bundle/toolchains/mipsel-linux-android


