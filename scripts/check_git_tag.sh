#!/bin/bash
set -e

readsymlink() {
    if hash greadlink 2>/dev/null; then
        greadlink -f "$@"
    else
        readlink -f "$@"
    fi
}

#goto this script dir
if [[ -d .git ]]; then
    cd scripts/
else
    cd $(dirname $(readsymlink $0))
fi

COMMIT=$(git rev-parse HEAD)
GIT_TAG=$(git describe --tags --exact-match $COMMIT || :) #current tag or empty string
MAVEN_VERSION=$(grep ext.mavenVersion ../build.gradle | awk '{print $3}' | sed "s/['\"]//g")

if [[ $GIT_TAG != "" && $GIT_TAG != $MAVEN_VERSION ]]; then
    echo "Git tag '$GIT_TAG' must match maven version '$MAVEN_VERSION'"
    exit 1
fi 

if [[ $GIT_TAG != "" ]]; then
    echo "maven version and git tag are equal ($MAVEN_VERSION)"
fi
