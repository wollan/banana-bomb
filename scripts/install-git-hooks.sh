#!/bin/bash
set -ex

mkdir -p ../.git/hooks
ln -s ../../scripts/check_git_tag.sh ../.git/hooks/pre-push

