package se.wollan.bananabomb;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Test;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;

/**
 * Created by aw on 2017-05-15.
 */
public class RottenBananaTests {

    @Test
    public void construct_nullOriginal_throwsArgument() throws Exception {
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new RottenBanana(null);
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void getOriginal_nonNullOriginal_returnInstance() throws Exception {
        String orig = "stub-banana";
        RottenBanana sut = new RottenBanana(orig);

        Object actual = sut.getOriginalBanana();

        assertEquals(orig, actual);
    }

}