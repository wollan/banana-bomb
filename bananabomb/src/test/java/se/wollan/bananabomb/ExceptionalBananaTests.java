package se.wollan.bananabomb;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;

/**
 * Created by aw on 2017-05-21.
 */
public class ExceptionalBananaTests {


    @Test
    public void construct_nullBanana_throwsArgument() throws Exception {
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new ExceptionalBanana(new Exception(""), null);
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void construct_nullException_throwsArgument() throws Exception {
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new ExceptionalBanana(null, "");
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void getCausingBanana_nonNullBanana_returnInstance() throws Exception {
        String orig = "stub-banana";
        ExceptionalBanana sut = new ExceptionalBanana(new Exception(""), orig);

        Object actual = sut.getCausingBanana();

        assertEquals(orig, actual);
    }

    @Test
    public void getThrowable_nonNullException_returnInstance() throws Exception {
        Exception orig = new Exception("");
        ExceptionalBanana sut = new ExceptionalBanana(orig, "");

        Object actual = sut.getException();

        assertEquals(orig, actual);
    }

    @Test
    public void rethrow_canThrowCheckedExceptionsWithoutRuntimeWrapping() throws Exception {

        ExceptionalBanana sut;
        try {
            throw new IOException("write fail");
        } catch (IOException e) {
            sut = new ExceptionalBanana(e, "");
        }

        final ExceptionalBanana finalSut = sut;
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() {
                finalSut.rethrow(); //throws checked!
            }
        });

        assertThat(actual, instanceOf(IOException.class));
        assertEquals("write fail", actual.getMessage());
        assertNull(actual.getCause());
        assertThat(actual.getStackTrace().length, greaterThan(1));
    }
}