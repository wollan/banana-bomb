package se.wollan.bananabomb;

/**
 * Created by aw on 2017-05-14.
 */

public final class NullBombingRangeException extends BananaException {

    private final String canonicalNameOfBombingRange;

    public NullBombingRangeException(String canonicalNameOfBombingRange) {
        super("Bombing range "+canonicalNameOfBombingRange+" cannot be null");
        this.canonicalNameOfBombingRange = canonicalNameOfBombingRange;
    }

    public String getCanonicalNameOfBombingRange() {
        return canonicalNameOfBombingRange;
    }
}
