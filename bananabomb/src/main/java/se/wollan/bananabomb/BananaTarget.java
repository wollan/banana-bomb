package se.wollan.bananabomb;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate methods that will handle incoming bananas.
 */
@Target(ElementType.METHOD) // on method level
public @interface BananaTarget {
}
