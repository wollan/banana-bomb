package se.wollan.bananabomb;

/**
 * Used internally to propagate {@link ExceptionalBanana}s,
 * cannot be used in a banana bomb.
 */
public interface ExceptionalBananaDetonator {

    void detonate(ExceptionalBanana exceptionalBanana);
}
