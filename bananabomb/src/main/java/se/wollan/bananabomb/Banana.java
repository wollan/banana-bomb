package se.wollan.bananabomb;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Bananas are simple POJO message classes.
 * Immutability is recommended as they are sent between threads.
 */
@Documented
@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
public @interface Banana {
}
