package se.wollan.bananabomb;

/**
 * Exceptional banana will be detonated in the event of an exception
 * in a banana target <b>if and only if</b> there are at least one locked target that will handle it
 * when the exception occurs.
 * Which means that if there aren't any exceptional banana targets at all or there are but not locked
 * at the moment, the exception will not get caught and likely crash the app.
 * We think that is more appropriate than silently suppressing it, hiding potential bugs.
 * One approach is to register an always-locked global exception handler for every banana bomb
 * in PROD mode and let the app crash in DEBUG mode.
 */
public final class ExceptionalBanana {

    private final Object causingBanana;
    private final Exception exception;

    public ExceptionalBanana(Exception exception, Object causingBanana) {
        if (exception == null) {
            throw new IllegalArgumentException("exception cannot be null");
        }
        if (causingBanana == null) {
            throw new IllegalArgumentException("causingBanana cannot be null");
        }

        this.exception = exception;
        this.causingBanana = causingBanana;
    }

    public Throwable getException() {
        return exception;
    }

    public Object getCausingBanana() {
        return causingBanana;
    }

    public void rethrow() {
        this.<RuntimeException>sneakyRethrow();
    }

    @SuppressWarnings("unchecked")
    private <T extends Exception> void sneakyRethrow() throws T {
        throw (T) exception;
    }

}
