package se.wollan.bananabomb;

/**
 * A detonated banana rots when it doesn't find any targets.
 * The rotten banana is then re-detonated matching any rotten targets out there!
 * (Dead letter functionality.)
 */
public final class RottenBanana {

    private final Object originalBanana;

    public RottenBanana(Object originalBanana) {
        if (originalBanana == null) {
            throw new IllegalArgumentException("originalBanana cannot be null");
        }

        this.originalBanana = originalBanana;
    }

    public Object getOriginalBanana() {
        return originalBanana;
    }
}
