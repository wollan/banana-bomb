package se.wollan.bananabomb;

/**
 * Created by aw on 2017-05-14.
 */

public final class NullBananaException extends BananaException {

    private final String canonicalNameOfBanana;

    public NullBananaException(String canonicalNameOfBanana) {
        super("Bombing range "+ canonicalNameOfBanana +" cannot be null");
        this.canonicalNameOfBanana = canonicalNameOfBanana;
    }

    public String getCanonicalNameOfBanana() {
        return canonicalNameOfBanana;
    }
}
