package se.wollan.bananabomb;

/**
 * Created by aw on 2017-04-30.
 */
public final class BombingRangeAlreadyLockedException extends BananaException {

    private final String canonicalNameOfBombingRange;

    public BombingRangeAlreadyLockedException(String canonicalNameOfBombingRange) {
        super("Cannot lock " + canonicalNameOfBombingRange + ", it is already locked!");
        this.canonicalNameOfBombingRange = canonicalNameOfBombingRange;
    }

    public String getCanonicalNameOfBombingRange() {
        return canonicalNameOfBombingRange;
    }
}
