package se.wollan.bananabomb;

/**
 * Created by aw on 2017-04-30.
 */
public final class BombingRangeAlreadyUnlockedException extends BananaException {

    private final String canonicalNameOfBombingRange;

    public BombingRangeAlreadyUnlockedException(String canonicalNameOfBombingRange) {
        super("Cannot unlock " + canonicalNameOfBombingRange + ", it is already unlocked!");
        this.canonicalNameOfBombingRange = canonicalNameOfBombingRange;
    }

    public String getCanonicalNameOfBombingRange() {
        return canonicalNameOfBombingRange;
    }
}
