package se.wollan.bananabomb;

/**
 * Created by aw on 2017-05-14.
 */

public final class UnsupportedTargetThreadException extends BananaException {

    private final String canonicalNameOfBombingRange;

    public UnsupportedTargetThreadException(String canonicalNameOfBombingRange) {
        super("Error while locking targets for "+canonicalNameOfBombingRange+
              ". The current thread (" + Thread.currentThread() + ") is not supported as a target thread, " +
              "it must be a android.os.Looper enabled thread like the UI thread or android.os.HandlerThread.");
        this.canonicalNameOfBombingRange = canonicalNameOfBombingRange;
    }

    public String getCanonicalNameOfBombingRange() {
        return canonicalNameOfBombingRange;
    }
}
