package se.wollan.bananabomb;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate the message bus interface of which implementation will be generated.
 */
@Target(ElementType.TYPE)
public @interface BananaBomb {
}
