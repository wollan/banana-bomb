package se.wollan.bananabomb;

/**
 * Created by aw on 2017-05-14.
 */

public class BananaException extends RuntimeException {

    public BananaException(String message) {
        super(message);
    }
}
