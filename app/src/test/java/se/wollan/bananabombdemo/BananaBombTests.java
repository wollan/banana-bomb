package se.wollan.bananabombdemo;

import android.os.Looper;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import se.wollan.bananabomb.BombingRangeAlreadyLockedException;
import se.wollan.bananabomb.BombingRangeAlreadyUnlockedException;
import se.wollan.bananabomb.ExceptionalBanana;
import se.wollan.bananabomb.ExceptionalBananaDetonator;
import se.wollan.bananabomb.NullBananaException;
import se.wollan.bananabomb.NullBombingRangeException;
import se.wollan.bananabomb.RottenBanana;
import se.wollan.bananabomb.UnsupportedTargetThreadException;
import se.wollan.bananabombdemo.abombs.CatchAllWildcardRange;
import se.wollan.bananabombdemo.abombs.EmptyBananaBomb;
import se.wollan.bananabombdemo.abombs.EmptyBananaBombImpl;
import se.wollan.bananabombdemo.abombs.EmptyBananaBombingRangeRadar;
import se.wollan.bananabombdemo.abombs.EmptyBananaBombingRange;
import se.wollan.bananabombdemo.abombs.ExceptionHandler;
import se.wollan.bananabombdemo.abombs.RottenBomb;
import se.wollan.bananabombdemo.abombs.RottenBombingRange;
import se.wollan.bananabombdemo.abombs.SingleBanana;
import se.wollan.bananabombdemo.abombs.SingleBananaBomb;
import se.wollan.bananabombdemo.abombs.SingleBombingRange;
import se.wollan.bananabombdemo.abombs.SingleBomb;
import se.wollan.bananabombdemo.abombs.SameNameBananaBomb;
import se.wollan.bananabombdemo.abombs.SameNameBananaBombImpl;
import se.wollan.bananabombdemo.abombs.SingleBombImpl;
import se.wollan.bananabombdemo.abombs.SingleBananaBombImpl;
import se.wollan.bananabombdemo.abombs.SingleBombingRangeBananaHandler;
import se.wollan.bananabombdemo.abombs.RottenBombingRangeBananaHandler;
import se.wollan.bananabombdemo.abombs.EmptyBananaBombingRangeBananaHandler;
import se.wollan.bananabombdemo.abombs.ExceptionHandlerBananaHandler;
import se.wollan.bananabombdemo.abombs.CatchAllWildcardRangeBananaHandler;
import se.wollan.bananabombdemo.abombs.SinglePlusWildcardBombImpl;
import se.wollan.bananabombdemo.abombs.RottenBombImpl;
import se.wollan.bananabombdemo.abombs.RangeWithTwoSingle;
import se.wollan.bananabombdemo.abombs.SinglePlusWildcardBomb;

import static javax.xml.xpath.XPathFactory.newInstance;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static se.wollan.bananabombdemo.assertutils.JsonMatcher.eqJsonOf;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import java.lang.reflect.Field;
import java.util.LinkedList;

@PrepareForTest({
        Looper.class,
        SingleBananaBombImpl.class,
        RottenBombImpl.class,
        SinglePlusWildcardBombImpl.class
})
public class BananaBombTests {

    public @Rule PowerMockRule rule = new PowerMockRule();

    @Before
    public void mockLooper() {
        PowerMockito.mockStatic(Looper.class);
        when(Looper.myLooper()).thenReturn(mock(Looper.class));
    }

    @Test
    public void lockTargets_twice_throwsAlreadyTargetLocked() throws Exception {
        final SingleBomb sut = new SingleBombImpl();
        final EmptyBananaBombingRange bombingRange = new EmptyBananaBombingRange();

        sut.lockTargets(bombingRange);
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                sut.lockTargets(bombingRange);
            }
        });

        assertThat(actual, instanceOf(BombingRangeAlreadyLockedException.class));
        assertThat(actual.getMessage(), containsString(EmptyBananaBombingRange.class.getCanonicalName()));
        assertEquals(EmptyBananaBombingRange.class.getCanonicalName(),
                ((BombingRangeAlreadyLockedException)actual).getCanonicalNameOfBombingRange());
    }

    @Test
    public void lockTargets_returnsSameInstance() throws Exception {
        final SingleBomb sut = new SingleBombImpl();
        final EmptyBananaBombingRange bombingRange = new EmptyBananaBombingRange();

        EmptyBananaBombingRangeRadar actual = sut.lockTargets(bombingRange);

        assertSame(sut, actual);
    }

    @Test
    public void lockTargets_withNullBombingRange_throwNullBombingRangeException() throws Exception {
        final SingleBomb sut = new SingleBombImpl();

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                sut.lockTargets(null);
            }
        });

        assertThat(actual, instanceOf(NullBombingRangeException.class));
        assertEquals(EmptyBananaBombingRange.class.getCanonicalName(),
                ((NullBombingRangeException)actual).getCanonicalNameOfBombingRange());
    }

    @Test
    public void lockTargets_onWithNullLooper_throwsUnsupportedTargetThreadException() throws Exception {
        final SingleBomb sut = new SingleBombImpl();
        final EmptyBananaBombingRange bombingRange = new EmptyBananaBombingRange();

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                sut.lockTargets(bombingRange, null);
            }
        });

        assertThat(actual, instanceOf(UnsupportedTargetThreadException.class));
        assertEquals(EmptyBananaBombingRange.class.getCanonicalName(),
                ((UnsupportedTargetThreadException)actual).getCanonicalNameOfBombingRange());
    }

    @Test
    public void unlockTargets_twice_throwsAlreadyTargetUnLocked() throws Exception {
        final SingleBomb sut = new SingleBombImpl();
        final EmptyBananaBombingRange bombingRange = new EmptyBananaBombingRange();

        EmptyBananaBombingRangeRadar localRadar = sut.lockTargets(bombingRange);
        localRadar.unlockTargets(bombingRange);
        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                sut.unlockTargets(bombingRange);
            }
        });

        assertThat(actual, instanceOf(BombingRangeAlreadyUnlockedException.class));
        assertThat(actual.getMessage(), containsString(EmptyBananaBombingRange.class.getCanonicalName()));
        assertEquals(EmptyBananaBombingRange.class.getCanonicalName(),
                ((BombingRangeAlreadyUnlockedException)actual).getCanonicalNameOfBombingRange());
    }

    @Test
    public void unlockTargets_withNullBombingRange_throwNullBombingRangeException() throws Exception {
        final SingleBomb sut = new SingleBombImpl();

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                sut.unlockTargets(null);
            }
        });

        assertThat(actual, instanceOf(NullBombingRangeException.class));
        assertEquals(EmptyBananaBombingRange.class.getCanonicalName(),
                ((NullBombingRangeException)actual).getCanonicalNameOfBombingRange());
    }

    @Test
    public void lockTargets_twoBombingRangesWithSameSimpleName_throwsNot() throws Exception {

        se.wollan.bananabombdemo.abombs.x.SameNameBombingRange xrange = new se.wollan.bananabombdemo.abombs.x.SameNameBombingRange();
        se.wollan.bananabombdemo.abombs.y.SameNameBombingRange yrange = new se.wollan.bananabombdemo.abombs.y.SameNameBombingRange();
        SameNameBananaBomb sameNameBomb = new SameNameBananaBombImpl();

        sameNameBomb.lockTargets(xrange);
        sameNameBomb.lockTargets(yrange);
    }

    @Test
    public void emptyBananaBombImpl_shouldOnlyImplementEmptyBananaBomb() throws Exception {
        EmptyBananaBombImpl sut = new EmptyBananaBombImpl();

        Class<?>[] interfaces = sut.getClass().getInterfaces();

        assertEquals(1, interfaces.length);
        assertEquals(EmptyBananaBomb.class.getCanonicalName(), interfaces[0].getCanonicalName());
    }

    @Test
    public void detonate_withOneLockedTarget_shouldCallTargetHandler() throws Exception {
        SingleBanana banana = new SingleBanana();
        SingleBananaBomb sut = new SingleBananaBombImpl();
        SingleBombingRange bombingRange = new SingleBombingRange();
        SingleBombingRangeBananaHandler handlerMock = mock(SingleBombingRangeBananaHandler.class);
        when(handlerMock.send(banana)).thenReturn(1);
        whenNew(SingleBombingRangeBananaHandler.class)
                .withAnyArguments()
                .thenReturn(handlerMock);
        sut.lockTargets(bombingRange);

        int sent = sut.detonate(banana);

        verify(handlerMock).send(eq(banana));
        assertEquals(1, sent);
    }

    @Test
    public void detonate_withTwoLockedTargets_shouldCallBothTargetHandlers() throws Exception {
        SingleBanana banana = new SingleBanana();
        SingleBananaBomb sut = new SingleBananaBombImpl();
        SingleBombingRange bombingRange1 = new SingleBombingRange();
        SingleBombingRange bombingRange2 = new SingleBombingRange();
        SingleBombingRangeBananaHandler handlerMock = mock(SingleBombingRangeBananaHandler.class);
        when(handlerMock.send(banana)).thenReturn(1);
        whenNew(SingleBombingRangeBananaHandler.class)
                .withAnyArguments()
                .thenReturn(handlerMock);
        sut.lockTargets(bombingRange1);
        sut.lockTargets(bombingRange2);

        int sent = sut.detonate(banana);

        verify(handlerMock, times(2)).send(eq(banana));
        assertEquals(2, sent);
    }

    @Test
    public void detonate_withOneLockedThenUnlockedTarget_shouldNotCallTargetHandler() throws Exception {
        SingleBanana banana = new SingleBanana();
        SingleBananaBomb sut = new SingleBananaBombImpl();
        SingleBombingRange bombingRange = new SingleBombingRange();
        SingleBombingRangeBananaHandler handlerMock = mock(SingleBombingRangeBananaHandler.class);
        when(handlerMock.send(banana)).thenReturn(1);
        when(handlerMock.getBombingRange()).thenReturn(bombingRange);
        whenNew(SingleBombingRangeBananaHandler.class)
                .withAnyArguments()
                .thenReturn(handlerMock);
        sut.lockTargets(bombingRange);
        sut.unlockTargets(bombingRange);

        int sent = sut.detonate(banana);

        verify(handlerMock, never()).send(eq(banana));
        assertEquals(0, sent);
    }

    @Test
    public void detonate_withNullBanana_throwsNullBananaException() throws Exception {
        final SingleBananaBomb sut = new SingleBananaBombImpl();
        final SingleBanana banana = null;

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                sut.detonate(banana);
            }
        });

        assertThat(actual, instanceOf(NullBananaException.class));
        assertEquals(SingleBanana.class.getCanonicalName(),
                ((NullBananaException)actual).getCanonicalNameOfBanana());
    }

    @Test
    public void detonate_bananaWithOnlyRottenTargetLocked_reRedetonatesAsRotten() throws Exception {
        SingleBanana banana = new SingleBanana("apa");
        RottenBombingRange bombingRange = new RottenBombingRange();
        RottenBomb sut = new RottenBombImpl();
        RottenBombingRangeBananaHandler handlerMock = mock(RottenBombingRangeBananaHandler.class);
        when(handlerMock.send(notNull(RottenBanana.class))).thenReturn(1);
        when(handlerMock.getBombingRange()).thenReturn(bombingRange);
        whenNew(RottenBombingRangeBananaHandler.class)
                .withAnyArguments()
                .thenReturn(handlerMock);
        sut.lockTargets(bombingRange);

        sut.detonate(banana);

        verify(handlerMock).send(eqJsonOf(new RottenBanana(banana)));
    }

    @Test
    public void detonate_exceptionalWithWildcardTarget_shouldNotHit() throws Exception {
        final SinglePlusWildcardBomb sut = new SinglePlusWildcardBombImpl();
        final EmptyBananaBombingRange bombingRange = new EmptyBananaBombingRange();
        sut.lockTargets(bombingRange);
        Field handlersField = SinglePlusWildcardBombImpl.class
                .getDeclaredField("se_wollan_bananabombdemo_abombs_EmptyBananaBombingRange_handlers");
        handlersField.setAccessible(true);
        Object handlers = handlersField.get(sut);
        EmptyBananaBombingRangeBananaHandler h = ((LinkedList<EmptyBananaBombingRangeBananaHandler>) handlers)
                .getFirst();
        Field exceptionalDetonatorField = EmptyBananaBombingRangeBananaHandler.class
                .getDeclaredField("exceptionalDetonator");
        exceptionalDetonatorField.setAccessible(true);
        ExceptionalBananaDetonator detonator = (ExceptionalBananaDetonator)
                exceptionalDetonatorField.get(h);
        ExceptionalBanana banana = new ExceptionalBanana(new Exception("FAIL!"), "");
        CatchAllWildcardRange catchAll = new CatchAllWildcardRange();
        CatchAllWildcardRangeBananaHandler handlerMock = mock(CatchAllWildcardRangeBananaHandler.class);
        when(handlerMock.send(banana)).thenReturn(1);
        whenNew(CatchAllWildcardRangeBananaHandler.class)
                .withAnyArguments()
                .thenReturn(handlerMock);
        ExceptionHandler exceptionHandler = new ExceptionHandler();
        ExceptionHandlerBananaHandler handlerMock1 = mock(ExceptionHandlerBananaHandler.class);
        when(handlerMock1.send(banana)).thenReturn(1);
        whenNew(ExceptionHandlerBananaHandler.class)
                .withAnyArguments()
                .thenReturn(handlerMock1);
        sut.lockTargets(catchAll);
        sut.lockTargets(exceptionHandler);

        detonator.detonate(banana);

        verify(handlerMock, never()).send(eq(banana));
    }


}

