package se.wollan.bananabombdemo;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import se.wollan.bananabombdemo.abombs.subbananas.AppMsgDetonator;
import se.wollan.bananabombdemo.abombs.subbananas.HomeMsgDetonator;
import se.wollan.bananabombdemo.abombs.subbananas.LoginMsgDetonator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.contains;

/**
 * Created by aw on 2017-06-11.
 */

public class DetonatorTests {

    @Test
    public void appMsgCluster_shouldHaveFourInterfaces() throws Exception {
        List<Class<?>> interfaces = Arrays.asList(AppMsgDetonator.class.getInterfaces());

        List<Class<?>> expected = Arrays.asList(
                se.wollan.bananabombdemo.abombs.subbananas.ServiceDestroyedDetonator.class,
                se.wollan.bananabombdemo.abombs.subbananas.SwapLoginVMDetonator.class,
                se.wollan.bananabombdemo.abombs.subbananas.SwapHomeVMDetonator.class,
                se.wollan.bananabombdemo.abombs.subbananas.UpdateVMDetonator.class
        );
        assertTrue(expected.containsAll(interfaces) && interfaces.containsAll(expected));
    }

    @Test
    public void homeMsgCluster_shouldHaveTwoInterfaces() throws Exception {
        List<Class<?>> interfaces = Arrays.asList(HomeMsgDetonator.class.getInterfaces());

        List<Class<?>> expected = Arrays.asList(
                se.wollan.bananabombdemo.abombs.subbananas.SwapHomeVMDetonator.class,
                se.wollan.bananabombdemo.abombs.subbananas.UpdateVMDetonator.class
        );
        assertTrue(expected.containsAll(interfaces) && interfaces.containsAll(expected));
    }

    @Test
    public void loginMsgCluster_shouldHaveTwoInterfaces() throws Exception {
        List<Class<?>> interfaces = Arrays.asList(LoginMsgDetonator.class.getInterfaces());

        List<Class<?>> expected = Arrays.asList(
                se.wollan.bananabombdemo.abombs.subbananas.SwapLoginVMDetonator.class,
                se.wollan.bananabombdemo.abombs.subbananas.UpdateVMDetonator.class
        );
        assertTrue(expected.containsAll(interfaces) && interfaces.containsAll(expected));
    }
}
