package se.wollan.bananabombdemo.assertutils;

import org.hamcrest.CustomTypeSafeMatcher;
import org.mockito.Matchers;

import static se.wollan.bananabombdemo.assertutils.Assert.toJson;

/**
 * Created by aw on 2017-05-15.
 */

public class JsonMatcher<T> extends CustomTypeSafeMatcher<T> {
    private final String expectedJson;

    private JsonMatcher(String expectedJson) {
        super(expectedJson);
        this.expectedJson = expectedJson;
    }

    @Override
    protected boolean matchesSafely(T actual) {
        String actualJson = toJson(actual);
        return expectedJson.equals(actualJson);
    }

    public static <T> T eqJsonOf(T expected) {
        String expectedJson = toJson(expected);
        return Matchers.argThat(new JsonMatcher<T>(expectedJson));
    }
}
