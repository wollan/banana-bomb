package se.wollan.bananabombdemo;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import java.util.Arrays;

import se.wollan.bananabomb.ExceptionalBanana;
import se.wollan.bananabomb.ExceptionalBananaDetonator;
import se.wollan.bananabomb.NullBombingRangeException;
import se.wollan.bananabomb.UnsupportedTargetThreadException;
import se.wollan.bananabombdemo.abombs.EmptyBananaBombingRange;
import se.wollan.bananabombdemo.abombs.ExceptionHandler;
import se.wollan.bananabombdemo.abombs.ExtendedSingleBanana;
import se.wollan.bananabombdemo.abombs.RangeWithTwoSingle;
import se.wollan.bananabombdemo.abombs.RangeWithTwoSingleBananaHandler;
import se.wollan.bananabombdemo.abombs.RangeWithWildcardTargetBananaHandler;
import se.wollan.bananabombdemo.abombs.EmptyBananaBombingRangeBananaHandler;
import se.wollan.bananabombdemo.abombs.ExceptionHandlerBananaHandler;
import se.wollan.bananabombdemo.abombs.RangeWithWildcardTarget;
import se.wollan.bananabombdemo.abombs.SingleBanana;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static se.wollan.bananabombdemo.assertutils.Assert.assertJsonEquals;

/**
 * Created by aw on 2017-05-20.
 */
@PrepareForTest({
        RangeWithTwoSingleBananaHandler.class,
        RangeWithWildcardTargetBananaHandler.class,
})
public class BananaHandlerTests {

    @Rule public PowerMockRule rule = new PowerMockRule();

    private static Handler handlerMock() throws Exception {
        Handler handlerMock = mock(Handler.class);
        whenNew(Handler.class)
                .withAnyArguments()
                .thenReturn(handlerMock);
        return handlerMock;
    }

    @Test
    public void send_bananaMatchingTwoTargets_shouldSendToBoth() throws Exception {
        final Looper looper = mock(Looper.class);
        ExceptionalBananaDetonator exceptionalBananaDetonator = mock(ExceptionalBananaDetonator.class);
        RangeWithTwoSingle bombingRange = new RangeWithTwoSingle();
        Handler handlerMock = handlerMock();
        RangeWithTwoSingleBananaHandler sut = new RangeWithTwoSingleBananaHandler(bombingRange, looper, exceptionalBananaDetonator);
        SingleBanana banana = new SingleBanana("apa");

        sut.send(banana);

        verify(handlerMock, times(2)).sendMessage(any(Message.class));
        verify(handlerMock).obtainMessage(eq(0), eq(banana));
        verify(handlerMock).obtainMessage(eq(1), eq(banana));
    }

    @Test
    public void send_bananaToWildcardRange_shouldSendToAllAssignableTargets() throws Exception {
        final Looper looper = mock(Looper.class);
        ExceptionalBananaDetonator exceptionalBananaDetonator = mock(ExceptionalBananaDetonator.class);
        RangeWithWildcardTarget bombingRange = new RangeWithWildcardTarget();
        Handler handlerMock = handlerMock();
        RangeWithWildcardTargetBananaHandler sut = new RangeWithWildcardTargetBananaHandler(bombingRange, looper, exceptionalBananaDetonator);
        ExtendedSingleBanana banana = new ExtendedSingleBanana("apa");

        sut.send(banana);

        verify(handlerMock, times(2)).sendMessage(any(Message.class));
        verify(handlerMock).obtainMessage(eq(0), eq(banana));
        verify(handlerMock).obtainMessage(eq(1), eq(banana));
    }

    @Test
    public void construct_nullLooper_throwsArgument() throws Exception {
        final EmptyBananaBombingRange range = new EmptyBananaBombingRange();

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new EmptyBananaBombingRangeBananaHandler(range, null, detonator);
            }
        });

        assertThat(actual, instanceOf(UnsupportedTargetThreadException.class));
    }

    final ExceptionalBananaDetonator detonator = new ExceptionalBananaDetonator() {
        @Override
        public void detonate(ExceptionalBanana exceptionalBanana) {

        }
    };

    @Test
    public void construct_nullBombingRange_throwsArgument() throws Exception {
        final Looper looper = mock(Looper.class);

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new EmptyBananaBombingRangeBananaHandler(null, looper, detonator);
            }
        });

        assertThat(actual, instanceOf(NullBombingRangeException.class));
    }

    @Test
    public void construct_nullDetonator_throwsArgument() throws Exception {
        final Looper looper = mock(Looper.class);
        final EmptyBananaBombingRange range = new EmptyBananaBombingRange();

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new EmptyBananaBombingRangeBananaHandler(range, looper, null);
            }
        });

        assertThat(actual, instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void construct_nullDetonatorOnRangeWithoutCatchTargets_DoesntThrow() throws Exception {
        final Looper looper = mock(Looper.class);
        final ExceptionHandler range = new ExceptionHandler();

        Throwable actual = catchThrowable(new ThrowableAssert.ThrowingCallable() {
            @Override
            public void call() throws Throwable {
                new ExceptionHandlerBananaHandler(range, looper, null);
            }
        });

        assertNull(actual);
    }

    @Test
    public void handleMessage_exceptionalMessage_callsBananaTarget() throws Exception {
        final Looper looper = mock(Looper.class);
        final ExceptionHandler range = new ExceptionHandler();
        ExceptionHandlerBananaHandler sut = new ExceptionHandlerBananaHandler(range, looper, null);
        ExceptionalBanana exceptionalBanana = new ExceptionalBanana(new Exception("fail"), "");
        Message msg = mock(Message.class);
        msg.what = -1;
        msg.obj = exceptionalBanana;

        sut.handleMessage(msg);

        assertJsonEquals(Arrays.asList(exceptionalBanana), range.caughtBananas);
    }
}
