package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaBomb;

/**
 * Created by aw on 2017-05-21.
 */
@BananaBomb
public interface RangeWithOnlyWildcardTargetBomb extends
        se.wollan.bananabombdemo.abombs.SingleBombingRangeRadar,
        se.wollan.bananabombdemo.abombs.ExtendedSingleBananaDetonator
{
}
