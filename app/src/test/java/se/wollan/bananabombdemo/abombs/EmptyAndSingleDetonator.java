package se.wollan.bananabombdemo.abombs;

/**
 * Created by aw on 2017-06-09.
 */

public interface EmptyAndSingleDetonator extends
    se.wollan.bananabombdemo.abombs.EmptyBananaDetonator,
    se.wollan.bananabombdemo.abombs.SingleBananaDetonator
{
}
