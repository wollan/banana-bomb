package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaBomb;
import se.wollan.bananabomb.BananaTarget;

@BananaBomb
public interface BananaBomb1 extends
        se.wollan.bananabombdemo.abombs.BombingRange1Radar,
        se.wollan.bananabombdemo.abombs.BombingRange2Radar,
        se.wollan.bananabombdemo.abombs.EmptyBananaDetonator { }

class BombingRange1 {
    @BananaTarget
    void on(EmptyBanana emptyBanana) { }
}

class BombingRange2 {
    @BananaTarget
    void on(EmptyBanana emptyBanana) { }
}
