package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.Banana;

/**
 * Created by aw on 2017-05-20.
 */
@Banana
public class ExtendedSingleBanana extends SingleBanana {

    public ExtendedSingleBanana() {
        super();
    }

    public ExtendedSingleBanana(String singleValue) {
        super(singleValue);
    }
}
