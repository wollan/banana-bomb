package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaBomb;

@BananaBomb
public interface SameNameBananaBomb extends
      se.wollan.bananabombdemo.abombs.EmptyBananaDetonator,
      se.wollan.bananabombdemo.abombs.x.SameNameBombingRangeRadar,
      se.wollan.bananabombdemo.abombs.y.SameNameBombingRangeRadar { }
