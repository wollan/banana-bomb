package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaBomb;

/**
 * Created by aw on 2017-06-09.
 */
@BananaBomb
public interface EmptyAndSingleBomb2 extends
        se.wollan.bananabombdemo.abombs.EmptyBananaDetonator,
        se.wollan.bananabombdemo.abombs.SingleBananaDetonator,

        se.wollan.bananabombdemo.abombs.EmptyAndSingleRadar
{
}
