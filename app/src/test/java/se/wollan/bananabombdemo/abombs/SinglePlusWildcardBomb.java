package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaBomb;

/**
 * Created by aw on 2017-05-11.
 */

@BananaBomb
public interface SinglePlusWildcardBomb extends
        se.wollan.bananabombdemo.abombs.ExceptionHandlerRadar,
        se.wollan.bananabombdemo.abombs.CatchAllWildcardRangeRadar,
        se.wollan.bananabombdemo.abombs.EmptyBananaDetonator,
        se.wollan.bananabombdemo.abombs.EmptyBananaBombingRangeRadar { }

