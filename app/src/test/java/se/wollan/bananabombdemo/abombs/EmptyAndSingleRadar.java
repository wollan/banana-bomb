package se.wollan.bananabombdemo.abombs;

/**
 * Created by aw on 2017-06-09.
 */

public interface EmptyAndSingleRadar extends
        se.wollan.bananabombdemo.abombs.SingleBombingRangeRadar,
        se.wollan.bananabombdemo.abombs.EmptyBananaBombingRangeRadar
{
}
