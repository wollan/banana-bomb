package se.wollan.bananabombdemo.abombs.subbananas;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by aw on 2017-06-11.
 */
@AppMsg
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface HomeMsg {
}
