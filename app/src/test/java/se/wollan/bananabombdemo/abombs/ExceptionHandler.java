package se.wollan.bananabombdemo.abombs;

import java.util.ArrayList;
import java.util.List;

import se.wollan.bananabomb.Banana;
import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabomb.ExceptionalBanana;

/**
 * Created by aw on 2017-05-24.
 */

public class ExceptionHandler {

    public List<ExceptionalBanana> caughtBananas = new ArrayList<>();

    @BananaTarget
    void on(ExceptionalBanana exceptionalBanana) {
        caughtBananas.add(exceptionalBanana);
    }
}
