package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.Banana;

@Banana
public class SingleBanana {

    private final String singleValue;

    public SingleBanana() {
        singleValue = null;
    }

    public SingleBanana(String singleValue) {
        this.singleValue = singleValue;
    }


}
