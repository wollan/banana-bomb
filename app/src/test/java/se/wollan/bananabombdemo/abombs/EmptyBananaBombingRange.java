package se.wollan.bananabombdemo.abombs;

import java.util.LinkedList;
import java.util.List;

import se.wollan.bananabomb.BananaTarget;

public class EmptyBananaBombingRange {
    public final List<EmptyBanana> emptyBananas = new LinkedList<>();

    @BananaTarget
    void on(EmptyBanana emptyBanana) {
        emptyBananas.add(emptyBanana);
    }
}

