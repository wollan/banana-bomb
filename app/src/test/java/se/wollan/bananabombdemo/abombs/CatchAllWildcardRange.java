package se.wollan.bananabombdemo.abombs;

import java.util.ArrayList;
import java.util.List;

import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabomb.ExceptionalBanana;

/**
 * Created by aw on 2017-05-25.
 */

public class CatchAllWildcardRange {

    public final List<Object> caught = new ArrayList<>();

    @BananaTarget
    void on(Object o) {
        caught.add(o);
    }
}
