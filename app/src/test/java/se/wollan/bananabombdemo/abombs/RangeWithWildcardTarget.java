package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaTarget;

/**
 * Created by aw on 2017-05-20.
 */

public class RangeWithWildcardTarget {

    @BananaTarget
    void on(SingleBanana banana) {}

    @BananaTarget
    void on(ExtendedSingleBanana banana) {}
}
