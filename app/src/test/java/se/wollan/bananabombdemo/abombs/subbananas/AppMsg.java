package se.wollan.bananabombdemo.abombs.subbananas;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import se.wollan.bananabomb.Banana;

/**
 * Created by aw on 2017-06-11.
 */
@Banana
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface AppMsg {
}
