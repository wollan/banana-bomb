package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.Banana;
import se.wollan.bananabomb.BananaBomb;
import se.wollan.bananabomb.BananaTarget;

@BananaBomb
public interface SingleBananaBomb extends
        se.wollan.bananabombdemo.abombs.SingleBananaDetonator,
        se.wollan.bananabombdemo.abombs.SingleBombingRangeRadar { }

