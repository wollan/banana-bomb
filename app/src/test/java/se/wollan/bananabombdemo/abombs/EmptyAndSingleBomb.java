package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaBomb;

/**
 * Created by aw on 2017-06-09.
 */
@BananaBomb
public interface EmptyAndSingleBomb extends
        se.wollan.bananabombdemo.abombs.EmptyAndSingleDetonator,
        se.wollan.bananabombdemo.abombs.SingleBombingRangeRadar,
        se.wollan.bananabombdemo.abombs.EmptyBananaBombingRangeRadar
{
}
