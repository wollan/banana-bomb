package se.wollan.bananabombdemo.abombs;

import se.wollan.bananabomb.BananaBomb;

/**
 * Created by aw on 2017-05-20.
 */
@BananaBomb
public interface RottenBomb extends
        se.wollan.bananabombdemo.abombs.RottenBombingRangeRadar,
        se.wollan.bananabombdemo.abombs.SingleBananaDetonator,
        se.wollan.bananabombdemo.abombs.SingleBombingRangeRadar
{
}
