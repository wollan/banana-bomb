package se.wollan.bananabombdemo.abombs.y;

import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabombdemo.abombs.EmptyBanana;

/**
 * Created by aw on 2017-05-13.
 */

public class SameNameBombingRange {

    @BananaTarget
    void on(EmptyBanana banana) { }
}

