package se.wollan.bananabombdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Date;

import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabomb.R;
import se.wollan.bananabomb.RottenBanana;
import se.wollan.bananabombdemo.bananas.ActivityDestroyed;
import se.wollan.bananabombdemo.bananas.ActivityDestroyedDetonator;
import se.wollan.bananabombdemo.bananas.Ping;
import se.wollan.bananabombdemo.bananas.PingDetonator;
import se.wollan.bananabombdemo.bananas.Pong;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    //just so constructor runs


    PingDetonator pingDetonator = AppGlobalBomb.INSTANCE;
    MainActivityRadar radar = AppGlobalBomb.INSTANCE;
    ActivityDestroyedDetonator activityDestroyedYesDetonator = AppGlobalBomb.INSTANCE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radar.lockTargets(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pingDetonator.detonate(new Ping());
    }

    @BananaTarget
    public void on(Pong pong) {
        Log.d(TAG, Thread.currentThread().getName() + " > yes ping-pong succeeds! time: " +
                (new Date().getTime() - pong.getPing().getSent().getTime()) + "ms");
    }

    @BananaTarget
    public void on(ActivityDestroyed destroyedYes) {
        //ignore
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        radar.unlockTargets(this);
        activityDestroyedYesDetonator.detonate(new ActivityDestroyed());
    }

    @BananaTarget
    public void on(RottenBanana rottenBanana) {
        Log.w(TAG, "We've got a rotten banana of type "+rottenBanana.getOriginalBanana()+" nobody wants!");
    }

}
