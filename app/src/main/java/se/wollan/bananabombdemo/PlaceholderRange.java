package se.wollan.bananabombdemo;

import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabomb.ExceptionalBanana;
import se.wollan.bananabombdemo.bananas.NoReceiversEvent;

/**
 * Created by aw on 2017-05-18.
 */

public class PlaceholderRange {

    public PlaceholderRange() {
    }

    @BananaTarget
    void on(NoReceiversEvent event) { }

    @BananaTarget
    void on(ExceptionalBanana exceptionalBanana) { }
}
