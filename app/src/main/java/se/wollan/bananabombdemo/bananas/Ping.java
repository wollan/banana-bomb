package se.wollan.bananabombdemo.bananas;

import java.util.Date;

import se.wollan.bananabomb.Banana;
import se.wollan.bananabombdemo.AppGlobalBomb;
import se.wollan.bananabombdemo.MainActivity;
import se.wollan.bananabombdemo.MainActivityRadar;

/**
 * Created by aw on 2017-04-20.
 */
@Banana
public class Ping implements Message {

    private final Date sent;

    public Ping() {
        sent = new Date();
    }

    public Date getSent() {
        return sent;
    }
}
