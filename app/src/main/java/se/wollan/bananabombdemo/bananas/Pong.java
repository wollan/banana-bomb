package se.wollan.bananabombdemo.bananas;

import java.util.Date;

import se.wollan.bananabomb.Banana;

/**
 * Created by aw on 2017-04-20.
 */
@Banana
public class Pong implements Message {

    private final Date sent;
    private final Ping ping;

    public Pong(Ping ping) {
        this.ping = ping;
        sent = new Date();
    }

    public Date getSent() {
        return sent;
    }

    public Ping getPing() {
        return ping;
    }

}
