package se.wollan.bananabombdemo;

import android.util.Log;

import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabombdemo.bananas.Message;

/**
 * Created by aw on 2017-05-21.
 */

public class EventLogger {

    private static final String TAG = EventLogger.class.getSimpleName();

    EventLoggerRadar radar;

    public EventLogger(Radar radar) {
        this.radar = radar.lockTargets(this);
    }

    @BananaTarget
    void logMessage(Object message) {
        if (message instanceof Message) {
            Log.d(TAG, "message " + message + " was sent");
        } else {
            Log.d(TAG, "a non-message " + message + " was sent");
        }
    }
}
