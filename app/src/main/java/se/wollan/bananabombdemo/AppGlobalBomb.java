package se.wollan.bananabombdemo;

import se.wollan.bananabomb.BananaBomb;

@BananaBomb
public interface AppGlobalBomb extends
        se.wollan.bananabombdemo.Radar,
        se.wollan.bananabombdemo.Detonator
{
    AppGlobalBomb INSTANCE = new AppGlobalBombImpl();
}




