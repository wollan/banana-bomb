package se.wollan.bananabombdemo;

import android.util.Log;

import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabomb.BuildConfig;
import se.wollan.bananabomb.ExceptionalBanana;

/**
 * Created by aw on 2017-05-22.
 */

public class GlobalExceptionHandler {

    private static final String TAG = GlobalExceptionHandler.class.getSimpleName();

    public GlobalExceptionHandler(GlobalExceptionHandlerRadar radar) {
        radar.lockTargets(this);
    }

    @BananaTarget
    void on(ExceptionalBanana exception) {

        Log.e(TAG, "Oooops! Houston, we've got a problem: " + exception.getException().getMessage());

        if (BuildConfig.DEBUG) {
            exception.rethrow();
        }
    }
}
