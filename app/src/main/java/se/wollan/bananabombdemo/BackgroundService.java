package se.wollan.bananabombdemo;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import java.io.IOException;

import se.wollan.bananabomb.Banana;
import se.wollan.bananabomb.BananaTarget;
import se.wollan.bananabombdemo.bananas.ActivityDestroyed;
import se.wollan.bananabombdemo.bananas.Message;
import se.wollan.bananabombdemo.bananas.NoReceiversEvent;
import se.wollan.bananabombdemo.bananas.NoReceiversEventDetonator;
import se.wollan.bananabombdemo.bananas.Ping;
import se.wollan.bananabombdemo.bananas.Pong;
import se.wollan.bananabombdemo.bananas.PongDetonator;


/**
 * Created by aw on 2017-04-23.
 */

public class  BackgroundService implements Handler.Callback {
    private static final String TAG = BackgroundService.class.getSimpleName();

    private final BackgroundServiceRadar radar;
    private final PongDetonator detonator;
    private final NoReceiversEventDetonator noReceiversEventDetonator;
    private final Detonator globalDetonator;
    private final HandlerThread handlerThread;

    public BackgroundService(BackgroundServiceRadar radar, PongDetonator detonator,
                             NoReceiversEventDetonator noReceiversEventDetonator, Detonator globalDetonator) {
        this.radar = radar;
        this.detonator = detonator;
        this.noReceiversEventDetonator = noReceiversEventDetonator;
        this.globalDetonator = globalDetonator;

        handlerThread = new HandlerThread("svc-thread", Thread.MIN_PRIORITY);
        handlerThread.start();
        radar.lockTargets(this, handlerThread.getLooper());
    }

    @BananaTarget
    public void on(Ping ping) {
        Log.d(TAG, Thread.currentThread().getName() + " > ping received! " + ping.getSent());
        Pong pong = new Pong(ping);
        detonator.detonate(pong);
        noReceiversEventDetonator.detonate(new NoReceiversEvent()); //banana will rot!
        globalDetonator.detonate((Message) pong); //will only hit message targets not pong targets

        new Handler(this).sendEmptyMessageDelayed(42, 3000);
    }

    @Override
    public boolean handleMessage(android.os.Message msg) {
        globalDetonator.detonate(new InternalMessage());
        return true;
    }

    @BananaTarget
    void on(InternalMessage internalMessage) throws IOException {
        throw new IOException("oh-oh, write did not go so well...");
    }

    @BananaTarget
    public void on(ActivityDestroyed event) {
        radar.unlockTargets(this);
    }
}

@Banana
class InternalMessage { }
