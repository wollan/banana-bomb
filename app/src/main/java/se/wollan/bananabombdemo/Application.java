package se.wollan.bananabombdemo;

import android.util.Log;

/**
 * Created by aw on 2017-05-22.
 */

public class Application extends android.app.Application {

    private static final String TAG = Application.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, " >>>>>> starting app <<<<<< ");

        AppGlobalBomb bomb = AppGlobalBomb.INSTANCE;

        new BackgroundService(bomb, bomb, bomb, bomb);
        new EventLogger(bomb);
        new GlobalExceptionHandler(bomb);
    }
}
